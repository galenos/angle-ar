# AngleAR

## Development Environment Setup

**!!!** Please ensure all of these things are installed, and that your environment variables are properly set **!!!**

#### The Basics
- [git](https://git-scm.com/)
 - If you don't know what this is, turn away now

#### Backend Development Tools
- [Visual Studio Community](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx)
  - Be sure to select the "Visual C++" feature during installation
- [Kinect v2 SDK](https://www.microsoft.com/en-us/download/details.aspx?id=44561)
- [OpenCV 3.1](http://opencv.org/downloads.html)
  - Set `OPENCV_HOME` environment variable to `[OpenCV install directory]\build`
  - Add `%OPENCV_HOME%\bin` to your `PATH`

The Backend is coded in [Visual C++](https://msdn.microsoft.com/en-us/library/68td296t.aspx). This allows us to easily interface with Microsoft's [.NET Framework](https://msdn.microsoft.com/en-us/library/w0x726c2(v=vs.110).aspx), Windows Forms, and the Kinect SDK, while maintaining access to critical native libraries (OpenCV)

#### Moverio BT2000 Development Tools
- [IntelliJ IDEA Community](https://www.jetbrains.com/idea/download/)
  - **DO NOT** use Android Studio, it's build system is incompatible with this project
  - You will need to register the Java JDK and Android SDK Platform 23 to IntelliJ IDEA
    - From the start screen, go to 'Confiure' > 'Project Defaults' > 'Project Structure'
    - Select 'SDKs' in the left column
    - Click the green '+' button and follow the instructions to add the SDKs
- [Java 8 JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Android SDK](http://developer.android.com/sdk/index.html)
  - Set `ANDROID_HOME` environment variable to Android SDK install directory
  - Install the following using the SDK manager
    - Android SDK Platform-tools
    - Android 6.0 (API 23) SDK Platform
    - Google USB Driver
- [MOVERIO Pro BT2000 SDK](https://tech.moverio.epson.com/en/work/bt-2000/technical_information.html)
  - You don't need to do anything with this just yet...

#### Other schmancy stuff
You do not need to install these things, but they may be nice to in your personal development environment.

- You may want a decent text editor, like [Sublime Text](https://www.sublimetext.com/3)
- Perhaps a UNIX-like shell, [MSYS2](https://msys2.github.io/)

## Project Setup

#### Git

1. Fire up 'Git Bash' (search for it in the Start Menu)
2. `cd` into the directory you would like to work out of
3. `git clone [url]`
4. That's it! Well, not quite. If you're not sure why/how using git is an excellent idea. It is strongly suggested you familiarize yourself with the following:
  - [Getting Started with Git, also with some nice videos if you don't like reading](https://git-scm.com/doc)
  - [Pull requests](https://help.github.com/articles/using-pull-requests/)

#### The Backend Project

This setup is relatively straightforward. Once you've git-cloned the code repository, just open 'AngleAR.sln' from the 'backend' folder. If you've installed all the dependencies above and set your environment variables properly, this should compile and run with the Kinect out of the box.

#### The Moverio BT2000 Project

This part is fun...

Follow the instructions in the 'ADB driver settings' section of [Epson's documentation](https://tech.moverio.epson.com/en/work/bt-2000/pdf/developers_guide.pdf). **DO NOT** follow any other development setup instructions in this documentation, unless you prefer to build a _broken_ project.

Okay, good. Now you can open the project, everything else is setup correctly and should work like a charm.

Opening an IntelliJ IDEA project takes an extra step:

1. Open IntelliJ IDEA
2. Click 'Open' and navigate to the 'moverio-bt2000' folder in the code repository
3. Again, if you've properly set up your JDK and the Android SDK in IntelliJ IDEA, everything should build and run on the headset, right out of the box.

Why does it work?

Epson decided it was a good idea to override default Android SDK classes in their proprietary SDK. This is very, very bad practice. The build system [Gradle](http://gradle.org/) in the latest versions of Android Studio is incapable of accommodating this poor design decision.

If you right-click on the 'app' module and 'Open Module Settings', you will see that the H725Ctrl library (Epson's SDK) is loaded _on top_ of the Android SDK. This is a bad thing to do, which is why Gradle does not allow it, however it is necessary in order to work with the MOVERIO headset.

##### Things that don't work

One of the requirements in this project is for an image source to be capable of collecting color _and_ depth data simultaneously. This requirement allows object-recognition algorithms to better classify objects in a 3D environment using both 3D data, and traditional pattern and color information. The MOVERIO BT2000 headset is equipped with two parallel cameras. This allows the headset to collect both color and depth data, except it doesn't.

The MOVERIO camera API has a mode called EDOF. This mode is supposed to allow collection of color _and_ depth data simultaneously. However, as stated in the documentation, this mode is currently broken, and depth data is not able to be captured. A fix should be coming in an update.

For development purposes, there is also a depth-only mode which the camera may operate in. Comments in the source code indicate where this mode may be set.

One approach to solving this issue, is altering camera modes between color collection and depth collection. This does not work, due to more bugs in Epson's SDK, this causes the app to crash after collecting only a few frames of information.

## The Backend

The backend program in its current state is fairly simple, but is tooled with several useful utility classes so that future development can be made easier. It is coded in Visual C++ to allow interfacing beween .NET libraries (managed C++) and OpenCV (native C++)

The main function for the backend is located in Backend.cpp. The current operation of the backend program is as follows:

- Displays a window previewing color and depth data retrieved from some camera
- A screenshot button allows a user to save color and depth data for the purpose of training an object detection algorithm
- Displays, in user-readable format, the results retrieved from some object detection algorithm (presently unimplemented in this solution)

You will find three #define configuration directives in this file:

<table>
  <tr>
    <th>Directive</th>
    <th>Values</th>
    <th>Purpose</th>
  </tr>
  <tr>
    <td>SOURCE_MODE</td>
    <td>SOURCE_KINECT or SOURCE_BT2000</td>
    <td>Changes which camera source the backend is interfacing with (Kinect or Moverio BT2000)</td>
  </tr>
  <tr>
    <td>KINECT_DEPTH_MIN and KINECT_DEPTH_MAX</td>
    <td>Integer, 0-8000, KINECT_DEPTH_MIN < KINECT_DEPTH_MAX</td>
    <td>The depth information gathered from the Kinect comes in a 16-bit depth flavor. For image processing purposes, we require 8-bit depth. Because 8-bit depth has such a small resolution compared to 16-bit, it is ideal to map only a range of the raw depth data to 8-bit to maximize usefulness. These values determine the range of raw depth values which will be mapped to 8-bit depth data.</td>
  </tr>
</table>

## Interfacing with the Kinect

_**Note:** This section applies to the kinect projects located in the 'kinect-svm' directory_

**Setup**s

Programs Installed:
Microsoft Visual Studio 2015
Kinect for Windows SDK 2.0
SDL 1.2.15
GLEW 1.13.0

We used Microsoft Visual Studio 2015 as an IDE. The Kinect use the headers windows.h and kinect.h. This is not automatically set up in Visual Studio so we must set up the Linker and make sure that the project will still be portable to other computers if necessary. SDL and GLEW are not used for the final product of the code but this will be useful if you decide to use them.

**Instructions for Linking Libraries in Visual Studio 2015:**

1. In the _Solution Explorer_ (defaulted to the right of the window) right click the _ProjectName_ and go to _Properties_

2. Under Configuration Properties click VC++ Directories

  1. Kinect: Under Include Directories add `$(KINECTSDK20_DIR)/include;` to the list of include directories already there,

  2. SDL: Open Windows Explorer and find your project folder. Create a new folder named `deps` in the same folder as your solution file (.sln). Open deps and create 2 new folders within named include and lib
  <br>After downloading SDL (link provided in Resources) extract the zip and open the include file within. Copy all of the header files within and paste them into your include folder in your deps folder.
  <br>Now go back to the Visual Studio (currently in VC++ Directories) and under Include Directories add `$(SolutionDir)deps\include` to include the SDL header files. 

  3. Kinect: Under Library Directories add `$(KINECTSDK20_DIR)Lib\x64;` to the list of libraries already there. (Make sure that the directory is correct. `$(KINECTSDK10_DIR)` is a macro that is predefined).

  4. SDL: After downloading SDL (link provided in Resources) extract the zip and open the x64 lib file. Copy all of the lib files (not the .dll files) and paste them into your lib folder in your deps folder.
  <br>Now go back to the Visual Studio (currently in VC++ Directories) and under Library Directories add $(SolutionDir)deps\lib to include the SDL lib files.

3. Under Configuration Properties click Linker then Input and under Additional Dependencies add kinect20.lib. Also add SDL.lib, SDLmain.lib if using SDL.

_Resource:_

SDL Setup Video: https://www.youtube.com/watch?v=FxCC9Ces1Yg

GLEW Setup Video: https://www.youtube.com/watch?v=u_NI7KOzyFM

**Project Code**

_Summary of Projects_

Color Screenshot aka. ColorBasics-D2D - This code is sample code from Microsoft that has not been edited. The purpose of this code is to complete these tasks: Display color data from the Kinect and save a frame by a click of the button.

Depth Screenshot aka. DepthBasics-D2D - This code is sample code from Microsoft that has not been edited. The purpose of this code is to complete these tasks: Display depth data from the Kinect and save a frame by a click of the button.

Kinect Stream aka. depthbasics-svm - This code is an edited version of Depth Screenshot that was edited to include both color and depth. The purpose of this code is to complete these tasks: Have depth data stored in a variable, have color data stored in a variable and display depth data for visibility. This is directly connected the SVM and is ready to provide data in real time.

_Explanation of Project Code_

I will only be explaining the Kinect Stream code because it is very similar to the Color Screenshot and the Depth Screenshot but edited to add multiple sources of frames.

Within `InitializeDefaultSensor()` you will find the code that initializes the Kinect Sensor. The code gets the default sensor then opens the frame reader for both depth and color. The frame reader will be the goto for reading any data from the Kinect.

Within `Update()` you will find the code that acquires the latest multi source frame and then references it either a depth or color frame. After that it calls the function `ProcessDepth` and `ProcessColor` and passes the parameter called `pBuffer` which currently holds all of the frame data. The `ProcessColor()` and `ProcessDepth()` functions then do some error checking and save `pBuffer` to the `RBGQuad` called `m_pDepthRGBX` or `m_pColorRGBX`. These two variables will later be inserted into the SVM for real time object recognition.

All of the other functions in this project are used to set up the window and message display. I would review them briefly but they are quite complicated.

**Resources**

_**Note:** This section applies to the 'opencvtest' and 'depthbasics-svm' projects located in the 'kinect-svm' directory_

Kinect basics and a good start: http://www.cs.princeton.edu/~edwardz/tutorials/index.html

Kinect Samples: https://msdn.microsoft.com/en-us/library/dn782040.aspx

Official Kinect Introduction: http://kinect.github.io/tutorial/

## Interfacing with the BT2000

Because the MOVERIO BT2000 runs on Android, collecting color and depth data from it is a bit more of a challenge. In order to accomplish this, a custom TCP protocol is used which operates over a bridged network connection with the help of the ADB forward command. To connect to the headset.

1. Connect the headset to the computer via USB. Ensure ADB has been properly configured as noted above
2. Start the AngleAR app on the headset
3. Start the Backend program
4. Color and (at present broken) depth information should be displayed in the backend program

The TCP protocol used between the headset and backend program uses a rudimentary messaging format. First, a string is sent distinguishing message type:

- TAG: Only sends a message tag
- JSON: Message is a JSON string
- RAW: Message contains raw binary data

Next, a string specifying the message tag is sent. This allows multiplexing between message handlers, for different message types. This feature allows more than just color and depth data to be streamed to/from the headset in the future.

Finally, if the message type is not TAG, the message body is sent. This is either JSON, or RAW data as specified by the message type.

All of this protocol is managed in the NetMessageSource (in the backend) and NetMessageServer (on the headset) classes. Simple methods for registering message handlers and sending/receiving the various message types are provided.

## Support Vector Machines

**OpenCV Installation:**

This process can be a little tricky, but between the OpenCV Installation documents and some Stack Overflow articles, you can get the library installed.  We used the executable installer rather than building from source.  Note that for the lab computer, the library must be installed to a location where it is visible to all users, and each user must set up the environment variables to access the library.  Once you think it is installed, start small by using the demo code provided by OpenCV to verify that your environment can use the library properly.

**OpenCV Versions:**

The project has been using OpenCV Version 2.4 and change.  Your team will likely want to port it to OpenCV 3.

**OpenCV Support Vector Machine (SVM):**

A SVM classifies data into the categories it has been trained upon based upon some rather complex math.  There can be only two categories, or there can be n (where n is greater than 2) categories.  The OpenCV SVM operates on OpenCV’s “Mat” Objects.  The first step of using an SVM is to train it upon the dataset.  In this process, you associate each data point with a category, and once all data points have a category, you call the method to train the SVM, and wait a while for the process to complete.  Once trained, a SVM should be saved using the appropriate method call.  This will produce a file which can be opened in another program to restore a trained SVM.  Note that with SVMs the size and format of both the data and the labels must be uniform both in training and in querying the SVM.

**Our Implementation:**

Our team used the OpenCV SVM to operate directly on images.  The images were sized uniformly, transformed into a format the SVM likes, and then fed into the SVM for training or querying.  This was done for both color images and depth-maps.  This is not something I would recommend the group that follows continues.  Instead I would recommend that you use a preprocessing algorithm on the images before feeding them into the SVM for training or querying.  We did not get into preprocessing, but the 2D team in the 2015-2016 year did.  Based upon our understanding of their experiences, we recommend trying to use HOG instead of something like SURF for preprocessing.

The training program we built is quite simple.  Each image (which has already been sized) is placed in a Mat Object.  The Mat Object is then transformed with a simple for loop.  Once the transformation is complete, the labels are set.  Once each image has been processed, the parameters for the SVM are set (you will want to consider tuning these) and the Mat objects containing the data and the corresponding labels are fed into the SVM for training.  This training code is found in the opencvtest Solution, as the Train.cpp file.

The detection program has been integrated into a modified Kinect driver.  This program translates the Kinect’s in-memory images into Mat Objects, splits the image into quadrants of the correct size, and then they are fed into the SVM for querying.  The result is saved into an integer, one integer for each quadrant for each of the two sources of data.  Once the SVM queries have returned results, a series of if statements are used to make the decision on what the object is.  Note that this integration effort was not immediately successful, and will require some tweaking before it will work properly for your team’s work.  This modified Kinect driver may be found in the depthbasics-d2d subdirectory of the Kinect Stream Folder, in the Kinect Steam Solution, as the MultiBasics.cpp file.

A similar effort was planned for the Moverio BT-2000, however it had some software issues that Epson had not resolved in time for our project.  They have indicated that these will be fixed with an update in the near future.

**Recommendations for the Next Team:**

Beware Murphy and his law.  Anything that can go wrong, will go wrong.  Do not procrastinate on this project, object recognition and augmented reality are both rather complicated.  Although our product this year is not fantastic, it is our hope that you will be able to use our work as a foundation for your own, and have more success.  Good luck.
