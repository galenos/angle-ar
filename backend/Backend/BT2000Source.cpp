#include "stdafx.h"
#include "BT2000Source.h"
#include "Adb.h"

using namespace System::Threading;
using namespace System::Runtime::InteropServices;

BT2000Source::BT2000Source() :
	NetMessageSource("localhost", 8900)
{
	MessageArrived[MESSAGE_TAG_FRAME] = gcnew MessageHandler(
		this, &BT2000Source::ProcessFrame);

	Thread ^connect = gcnew Thread(gcnew ThreadStart(
		this, &BT2000Source::ConnectLoop));
	connect->IsBackground = true;
	connect->Start();
}

void BT2000Source::BeginAcquireFrame()
{
	SendMessage(gcnew DevMessage(MESSAGE_TAG_FRAME));
	frameRequested = true;
}

void BT2000Source::ConnectLoop()
{
	while (true)
	{
		if (Adb::CheckDevice(SERIAL_NUMBER))
		{
			Adb::Execute(String::Format(
				"-s {0} forward tcp:{1} tcp:{2}", SERIAL_NUMBER, LOCAL_PORT, DEVICE_PORT));
			if (frameRequested) {
				SendMessage(gcnew DevMessage(MESSAGE_TAG_FRAME));
			}
			String ^status = Connect();
			Console::WriteLine("{0}, trying again...", status);
		}
		else
		{
			Console::WriteLine("Device not connected, trying again...");
		}
	}
}

void BT2000Source::ProcessFrame(DevMessage ^message)
{
	byte yuvData[COLOR_LENGTH];
	byte zData[DEPTH_LENGTH];
	Marshal::Copy(message->RawData, 0, IntPtr(&yuvData), COLOR_LENGTH);
	Marshal::Copy(message->RawData, COLOR_LENGTH, IntPtr(&zData), DEPTH_LENGTH);

	cv::Mat yuvMat(FRAME_HEIGHT * 3 / 2, FRAME_WIDTH, CV_8UC1, &yuvData);
	cv::Mat bgrMat(FRAME_HEIGHT, FRAME_WIDTH, CV_8UC3);
	cv::cvtColor(yuvMat, bgrMat, cv::ColorConversionCodes::COLOR_YUV2BGR_NV21);

	cv::Mat zMat(FRAME_HEIGHT, FRAME_WIDTH, CV_8UC1, &zData);

	frameRequested = false;
	FrameArrived(gcnew BgrzFrame(bgrMat, zMat));
}
