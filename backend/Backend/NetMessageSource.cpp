#include "stdafx.h"
#include "NetMessageSource.h"

using namespace System::IO;
using namespace System::Text;

static void WriteString(BinaryWriter ^writer, String ^text);
static void WriteBytes(BinaryWriter ^writer, array<Byte> ^buffer);

static String ^ReadString(BinaryReader ^reader);
static array<Byte> ^ReadBytes(BinaryReader ^reader);

void NetMessageSource::SendMessage(DevMessage ^message)
{
	messagePool->Add(message);
}

String ^NetMessageSource::Connect()
{
	try
	{
		client = gcnew TcpClient;
		client->Connect(host, port);

		BinaryReader ^reader = gcnew BinaryReader(client->GetStream());
		if (ReadString(reader) != CONNECT_MESSAGE)
			return "Connection verification failed";

		Thread ^read = gcnew Thread(gcnew ThreadStart(
			this, &NetMessageSource::SocketRead));
		read->IsBackground = true;
		Thread ^write = gcnew Thread(gcnew ThreadStart(
			this, &NetMessageSource::SocketWrite));
		write->IsBackground = true;

		read->Start();
		write->Start();

		read->Join();
		write->Interrupt();
		write->Join();

		return "Connection ended";
	}
	catch (Exception ^)
	{
		return "Connection failed";
	}
}

void NetMessageSource::SocketRead()
{
	try
	{
		BinaryReader ^reader = gcnew BinaryReader(client->GetStream());
		while (client->Connected)
		{
			MessageType type = (MessageType) Enum::Parse(MessageType::typeid, ReadString(reader));
			String ^tag = ReadString(reader);

			MessageHandler ^handler = events[tag];

			if (handler != nullptr)
			{
				DevMessage ^message = gcnew DevMessage(type, tag);
				switch (type)
				{
					case MessageType::TAG:
						break;
					case MessageType::JSON:
						message->Data = JObject::Parse(ReadString(reader));
						break;
					case MessageType::RAW:
						message->RawData = ReadBytes(reader);
						break;
				}
				handler(message);
			}
		}
	}
	catch (Exception ^) {}
}

void NetMessageSource::SocketWrite()
{
	try
	{
		BinaryWriter ^writer = gcnew BinaryWriter(client->GetStream());
		while (client->Connected)
		{
			DevMessage ^message = messagePool->Take();
			WriteString(writer, Enum::GetName(MessageType::typeid, message->Type));
			WriteString(writer, message->Tag);

			switch (message->Type)
			{
				case MessageType::TAG:
					break;
				case MessageType::JSON:
					WriteString(writer, message->Data->ToString(Newtonsoft::Json::Formatting::None));
					break;
				case MessageType::RAW:
					WriteBytes(writer, message->RawData);
					break;
			}
			writer->Flush();
		}
	}
	catch (Exception ^) {}
}

static void WriteString(BinaryWriter ^writer, String ^text)
{
	WriteBytes(writer, Encoding::UTF8->GetBytes(text));
}
static void WriteBytes(BinaryWriter ^writer, array<Byte> ^buffer)
{
	array<Byte> ^length = BitConverter::GetBytes(buffer->Length);
	Array::Reverse(length);
	writer->Write(length);
	writer->Write(buffer);
}

static String ^ReadString(BinaryReader ^reader)
{
	return Encoding::UTF8->GetString(ReadBytes(reader));
}
static array<Byte> ^ReadBytes(BinaryReader ^reader)
{
	array<Byte> ^buffer = reader->ReadBytes(4);
	Array::Reverse(buffer);
	int length = BitConverter::ToInt32(buffer, 0);
	return reader->ReadBytes(length);
}
