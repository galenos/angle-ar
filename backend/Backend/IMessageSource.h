#pragma once
#include "DevMessage.h"

using namespace System::Collections::Concurrent;

delegate void MessageHandler(DevMessage ^message);

typedef ConcurrentDictionary<String ^, MessageHandler ^> MessageHandlerDictionary;

interface class IMessageSource
{
public:

	property MessageHandlerDictionary ^MessageArrived {
		MessageHandlerDictionary ^get();
	}

	void SendMessage(DevMessage ^message);
};
