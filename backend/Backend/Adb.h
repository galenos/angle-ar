#pragma once

using namespace System;

ref class Adb
{
private:

	literal String ^adbPath = "%ANDROID_HOME%\\platform-tools";

public:

	// Send an ADB command
	static String ^Execute(String ^command);
	// Check if a particular ADB device is connected
	static bool CheckDevice(String ^serialNumber);
};
