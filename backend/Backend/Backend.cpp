// Backend.cpp : main project file.

#include "stdafx.h"
#include "Globals.h"

/*** Program Configuration ***/

#define SOURCE_KINECT 0
#define SOURCE_BT2000 1

/*
* Image data source
*/
#define SOURCE_MODE SOURCE_KINECT

/*
* Kinect-specific parameters
* Raw Kinect depth maps have 16-bit depth, OpenCV likes to operate on
*  8-bit depth maps. This defines the range over which to scale the
*  Kinect depth data (in millimeters)
*/
#define KINECT_DEPTH_MIN 860
#define KINECT_DEPTH_MAX 1120

/*** Main Program ***/

using namespace System;
using namespace msclr::interop;

void InitGlobals();
void BackendStart();

void FrameArrived(BgrzFrame ^frame);
delegate void ImageUpdate(Bitmap ^rgb, Bitmap ^z, String ^msg);
void ImageUpdateFunc(Bitmap ^rgb, Bitmap ^z, String ^msg);

void ScreenshotClick(Object ^o, EventArgs ^e);

static bool takeScreenshot = false;
static int screenshotNum = 0;

/*
* Initialize global variables
*/
void InitGlobals()
{
	Globals::Detect = gcnew ObjectDetect();
	Globals::Preview = gcnew PreviewWindow();

	switch (SOURCE_MODE)
	{
	case SOURCE_KINECT:
		Globals::Source = gcnew KinectSource(KINECT_DEPTH_MIN, KINECT_DEPTH_MAX);
		break;
	case SOURCE_BT2000:
		Globals::Source = gcnew BT2000Source();
		break;
	}
}

/*
* The backend heavy lifting thread
* Nothing too exciting here, go explore some of the classes it uses
*/
void BackendStart()
{
	// Begin collecting color/depth data
	Globals::Source->FrameArrived += gcnew FrameHandler(&FrameArrived);
	Globals::Source->BeginAcquireFrame();

	// Handle the screenshot button :D
	Globals::Preview->scBtn->Click += gcnew System::EventHandler(&ScreenshotClick);

	// Some CLI goodies
	String ^command;
	while (true)
	{
		command = Console::ReadLine();
		array<String ^> ^cmd = command->Split(' ');
		
		// Change kinect depth range e.g.
		//  kdepth 500 2000
		if (cmd[0]->Equals("kdepth") && cmd->Length == 3)
		{
			KinectSource ^source = dynamic_cast<KinectSource ^>(Globals::Source);
			int min, max;
			if (source != nullptr &&
				int::TryParse(cmd[1], min) &&
				int::TryParse(cmd[2], max))
			{	
				source->DepthMin = min;
				source->DepthMax = max;
			}
		}
		// Take a screenshot
		if (cmd[0]->Equals("sc")) {
			takeScreenshot = true;
		}
		// Terminate program
		if (cmd[0]->Equals("exit")) {
			Application::Exit();
		}
	}
}

void ImageUpdateFunc(Bitmap ^rgb, Bitmap ^z, String ^msg)
{
	// Update color and depth preview images
	Globals::Preview->bgrBox->Image = rgb;
	Globals::Preview->zBox->Image = z;
	Globals::Preview->statusLab->Text = msg;
}

void FrameArrived(BgrzFrame ^frame)
{
	// Acquire rgb and depth bitmaps
	Bitmap ^rgb = frame->GetBGRBitmap();
	Bitmap ^z = frame->GetZBitmap();
	// Acquire object detect status
	String ^msg = marshal_as<String ^>(Globals::Detect->ProcessFrame(frame->BGR, frame->Z));

	// Save screenshot if requested
	if (takeScreenshot)
	{
		Console::WriteLine("Screenshot!");
		takeScreenshot = false;
		frame->Save("sc" + screenshotNum++);
	}

	// Finally invoke GUI thread to update preview images & status
	if (Globals::Preview->InvokeRequired)
	{
		Globals::Preview->Invoke(gcnew ImageUpdate(&ImageUpdateFunc), rgb, z, msg);
	}
	else
	{
		ImageUpdateFunc(rgb, z, msg);
	}

	// Request another color/depth frame
	Globals::Source->BeginAcquireFrame();
}

void ScreenshotClick(Object ^o, EventArgs ^e) {
	// Raise screenshot request flag
	takeScreenshot = true;
}

int main(array<System::String ^> ^args)
{
	InitGlobals();

	// The current thread is going to be the GUI thread, start a new
	//  thread for all background processing goodness
	Thread ^processingThread = gcnew Thread(gcnew ThreadStart(&BackendStart));
	processingThread->IsBackground = true;
	processingThread->Start();

	Application::Run(Globals::Preview);
	return 0;
}
