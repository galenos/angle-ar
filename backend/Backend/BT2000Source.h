#pragma once
#include "NetMessageSource.h"
#include "IBgrzSource.h"

using namespace System;

ref class BT2000Source :
	public NetMessageSource,
	public IBgrzSource
{
private:

	literal String ^SERIAL_NUMBER = "0123456789ABCDEF";
	literal int LOCAL_PORT = 8900;
	literal int DEVICE_PORT = 8900;

	literal String ^MESSAGE_TAG_FRAME = "FRAME";
	literal int FRAME_WIDTH = 640;
	literal int FRAME_HEIGHT = 480;
	literal int COLOR_LENGTH = FRAME_HEIGHT * 3 / 2 * FRAME_WIDTH;
	literal int DEPTH_LENGTH = FRAME_HEIGHT * FRAME_WIDTH;

	bool frameRequested = false;

public:
	
	virtual event FrameHandler ^FrameArrived;

	BT2000Source();

	virtual void BeginAcquireFrame();

private:

	void ConnectLoop();
	void ProcessFrame(DevMessage ^message);
};
