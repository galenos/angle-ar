#include "stdafx.h"
#include "BgrzFrame.h"

using namespace msclr::interop;

BgrzFrame::BgrzFrame()
{
	bgr = new cv::Mat();
	z = new cv::Mat();
}
BgrzFrame::BgrzFrame(cv::Mat bgr, cv::Mat z) :
	BgrzFrame()
{
	*this->bgr = bgr;
	*this->z = z;
}

BgrzFrame::~BgrzFrame() { this->!BgrzFrame(); }
BgrzFrame::!BgrzFrame()
{
	delete bgr;
	delete z;
}

BgrzFrame ^BgrzFrame::Open(String ^fileName)
{
	return Open(marshal_as<std::string>(fileName));
}
BgrzFrame ^BgrzFrame::Open(std::string &fileName)
{
	cv::Mat bgr = cv::imread(fileName + ".bgr.bmp");
	cv::Mat z = cv::imread(fileName + ".z.bmp");
	return gcnew BgrzFrame(bgr, z);
}

BgrzFrame ^BgrzFrame::Open(String ^colorFile, String ^depthFile)
{
	return Open(
		marshal_as<std::string>(colorFile),
		marshal_as<std::string>(depthFile));
}
BgrzFrame ^BgrzFrame::Open(std::string &colorFile, std::string &depthFile)
{
	cv::Mat bgr = cv::imread(colorFile);
	cv::Mat z = cv::imread(depthFile);
	return gcnew BgrzFrame(bgr, z);
}

void BgrzFrame::Save(String ^fileName)
{
	Save(marshal_as<std::string>(fileName));
}
void BgrzFrame::Save(std::string &fileName)
{
	cv::imwrite(fileName + ".bgr.bmp", BGR);
	cv::imwrite(fileName + ".z.bmp", Z);
}

void BgrzFrame::Save(String ^colorFile, String ^depthFile)
{
	Save(
		marshal_as<std::string>(colorFile),
		marshal_as<std::string>(depthFile));
}
void BgrzFrame::Save(std::string &colorFile, std::string &depthFile)
{
	cv::imwrite(colorFile, BGR);
	cv::imwrite(depthFile, Z);
}

Bitmap ^BgrzFrame::GetBGRBitmap()
{
	if (BGR.type() != CV_8UC3) return nullptr;

	Imaging::PixelFormat format(PixelFormat::Format24bppRgb);

	Bitmap ^bmp = gcnew Bitmap(BGR.cols, BGR.rows, format);
	BitmapData ^data = bmp->LockBits(
		Drawing::Rectangle(0, 0, BGR.cols, BGR.rows),
		Imaging::ImageLockMode::WriteOnly,
		format);

	uchar *src = BGR.ptr();
	uchar *dst = (uchar *) data->Scan0.ToPointer();
	for (int row = 0; row < data->Height; ++row)
		memcpy(&dst[row * data->Stride], &src[row * BGR.step], data->Width * 3);

	bmp->UnlockBits(data);

	return bmp;
}
Bitmap ^BgrzFrame::GetZBitmap()
{
	if (Z.type() != CV_8UC1) return nullptr;

	Imaging::PixelFormat format = PixelFormat::Format8bppIndexed;

	Bitmap ^bmp = gcnew Bitmap(Z.cols, Z.rows, format);
	bmp->Palette = GrayscalePalette;
	BitmapData ^data = bmp->LockBits(
		Drawing::Rectangle(0, 0, Z.cols, Z.rows),
		Imaging::ImageLockMode::WriteOnly,
		format);

	uchar *src = Z.ptr();
	uchar *dst = (uchar *)data->Scan0.ToPointer();
	for (int row = 0; row < data->Height; ++row)
		memcpy(&dst[row * data->Stride], &src[row * Z.step], data->Width);

	bmp->UnlockBits(data);

	return bmp;
}
