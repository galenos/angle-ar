#pragma once
#include <string>
#include <opencv\cv.hpp>

ref class ObjectDetect
{
public:
	ObjectDetect();
	~ObjectDetect();

	std::string ProcessFrame(cv::Mat bgr, cv::Mat z);

protected:
	!ObjectDetect();
};
