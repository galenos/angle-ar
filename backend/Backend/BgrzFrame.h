#pragma once
#pragma unmanaged
#include <string>
#include <opencv\cv.hpp>
#pragma managed

using namespace System;
using namespace System::Drawing;
using namespace System::Drawing::Imaging;

/*
* A wrapper for unmanaged OpenCV Mat objects
* Also contains several fancy functions such as:
*  - Save color and depth images
*  - Load color and depth iamges
*  - Convert color and depth mats to Windows Bitmaps w/o memory leaks !!
*/
ref class BgrzFrame
{
private:
	
	cv::Mat *bgr;
	cv::Mat *z;

	static ColorPalette ^GrayscalePalette = GenerateGrayscalePalette();
	static ColorPalette ^GenerateGrayscalePalette()
	{
		Bitmap ^image = gcnew Bitmap(1, 1, PixelFormat::Format8bppIndexed);

		ColorPalette ^palette = image->Palette;
		for (int i = 0; i < palette->Entries->Length; ++i)
		{
			palette->Entries[i] = Color::FromArgb(i, i, i);
		}
		return palette;
	}

protected:

	!BgrzFrame();

public:

	BgrzFrame();
	BgrzFrame(cv::Mat bgr, cv::Mat z);

	~BgrzFrame();

	property cv::Mat BGR {
		cv::Mat get() { return *bgr; }
		void set(cv::Mat val) { *bgr = val; }
	}
	property cv::Mat Z {
		cv::Mat get() { return *z; }
		void set(cv::Mat val) { *z = val; }
	}

	static BgrzFrame ^Open(String ^fileName);
	static BgrzFrame ^Open(std::string &fileName);

	static BgrzFrame ^Open(String ^colorFile, String ^depthFile);
	static BgrzFrame ^Open(std::string &colorFile, std::string &depthFile);

	void Save(String ^fileName);
	void Save(std::string &fileName);

	void Save(String ^colorFile, String ^depthFile);
	void Save(std::string &colorFile, std::string &depthFile);

	Bitmap ^GetBGRBitmap();
	Bitmap ^GetZBitmap();
};
