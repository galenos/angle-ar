#pragma once
#include "IBgrzSource.h"

using namespace Microsoft::Kinect;

ref class KinectSource :
	public IBgrzSource
{
private:

	int depthMin;
	int depthMax;
	int depthD;

	KinectSensor ^sensor;
	MultiSourceFrameReader ^frameReader;

	bool frameRequested = false;

public:

	virtual event FrameHandler ^FrameArrived;

	property int DepthMin {
		int get() {
			return depthMin;
		}
		void set(int val) {
			depthMin = val;
			depthD = depthMax - depthMin;
		}
	}
	property int DepthMax {
		int get() {
			return depthMax;
		}
		void set(int val) {
			depthMax = val;
			depthD = depthMax - depthMin;
		}
	}

	KinectSource() : KinectSource(0, 2000) {}
	KinectSource(int rangeMin, int rangeMax);

	virtual void BeginAcquireFrame();

private:

	void ProcessFrame(Object ^sender, MultiSourceFrameArrivedEventArgs ^e);
};
