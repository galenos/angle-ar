#pragma once

using namespace System;
using namespace Newtonsoft::Json::Linq;

public enum class MessageType { TAG, JSON, RAW };

ref class DevMessage
{
private:

	MessageType type;
	String ^tag;
	JObject ^data;
	array<Byte> ^rawData;

public:

	DevMessage(MessageType type, String ^tag) :
		type(type), tag(tag) {};

	DevMessage(String ^tag) :
		type(MessageType::TAG), tag(tag) {};
	DevMessage(String ^tag, JObject ^data) :
		type(MessageType::JSON), tag(tag), data(data) {};
	DevMessage(String ^tag, array<Byte> ^data) :
		type(MessageType::RAW), tag(tag), rawData(data) {};

	property MessageType Type {
		MessageType get() {
			return type;
		}
	}
	property String ^Tag {
		String ^get() {
			return tag;
		}
	}
	property JObject ^Data {
		JObject ^get() {
			return data;
		}
		void set(JObject ^value) {
			data = value;
		}
	}
	property array<Byte> ^RawData {
		array<Byte> ^get() {
			return rawData;
		}
		void set(array<Byte> ^value) {
			rawData = value;
		}
	}
};
