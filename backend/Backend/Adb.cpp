#include "stdafx.h"
#include "Adb.h"

using namespace System::Diagnostics;
using namespace System::Text::RegularExpressions;

String ^Adb::Execute(String ^command)
{
	Process ^process = gcnew Process;

	process->StartInfo->UseShellExecute = false;
	process->StartInfo->RedirectStandardOutput = true;
	process->StartInfo->WorkingDirectory = Environment::ExpandEnvironmentVariables(adbPath);
	process->StartInfo->FileName = "cmd.exe";
	process->StartInfo->Arguments = "/c adb " + command;

	process->Start();
	process->WaitForExit();

	return process->StandardOutput->ReadToEnd();
}

bool Adb::CheckDevice(String ^serialNumber)
{
	String ^devices = Execute("devices");
	return Regex::IsMatch(devices, serialNumber + "\\s+device");
}
