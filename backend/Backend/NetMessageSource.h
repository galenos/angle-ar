#pragma once
#include "IMessageSource.h"

using namespace System;
using namespace System::Net::Sockets;
using namespace System::Threading;

ref class NetMessageSource : IMessageSource
{
private:

	literal String ^CONNECT_MESSAGE = "CONNECT";

	String ^host;
	int port;
	TcpClient ^client;

	MessageHandlerDictionary ^events;
	BlockingCollection<DevMessage ^> ^messagePool;

public:

	property MessageHandlerDictionary ^MessageArrived {
		virtual MessageHandlerDictionary ^get() { return events; }
	};

	virtual void SendMessage(DevMessage ^message);

protected:

	NetMessageSource(String ^host, int port) :
		host(host), port(port),
		events(gcnew MessageHandlerDictionary()),
		messagePool(gcnew BlockingCollection<DevMessage ^>()) {}

	String ^Connect();

private:

	void SocketRead();
	void SocketWrite();
};
