#pragma once

namespace Backend {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for PreviewWindow
	/// </summary>
	public ref class PreviewWindow : public System::Windows::Forms::Form
	{
	public:
		PreviewWindow(void)
		{
			InitializeComponent();

			ClientSize = Drawing::Size(1260, 440);
			FormBorderStyle = Forms::FormBorderStyle::FixedSingle;

			bgrBox->Location = Drawing::Point(0, 0);
			bgrBox->ClientSize = Drawing::Size(720, 405);
			bgrBox->SizeMode = PictureBoxSizeMode::Zoom;

			zBox->Location = Drawing::Point(720, 0);
			zBox->ClientSize = Drawing::Size(540, 405);
			zBox->SizeMode = PictureBoxSizeMode::Zoom;

			scBtn->Location = Drawing::Point(1155, 410);
			scBtn->ClientSize = Drawing::Size(100, 25);

			statusLab->Location = Drawing::Point(10, 415);
			statusLab->Text = "Loading...";
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~PreviewWindow()
		{
			if (components)
			{
				delete components;
			}
		}

	public: System::Windows::Forms::PictureBox^  bgrBox;
	public: System::Windows::Forms::PictureBox^  zBox;
	public: System::Windows::Forms::Button^  scBtn;
	public: System::Windows::Forms::Label^  statusLab;

	public:

	public:

	public:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->bgrBox = (gcnew System::Windows::Forms::PictureBox());
			this->zBox = (gcnew System::Windows::Forms::PictureBox());
			this->scBtn = (gcnew System::Windows::Forms::Button());
			this->statusLab = (gcnew System::Windows::Forms::Label());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bgrBox))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zBox))->BeginInit();
			this->SuspendLayout();
			// 
			// bgrBox
			// 
			this->bgrBox->Location = System::Drawing::Point(0, 0);
			this->bgrBox->Name = L"bgrBox";
			this->bgrBox->Size = System::Drawing::Size(100, 50);
			this->bgrBox->TabIndex = 0;
			this->bgrBox->TabStop = false;
			// 
			// zBox
			// 
			this->zBox->Location = System::Drawing::Point(100, 0);
			this->zBox->Name = L"zBox";
			this->zBox->Size = System::Drawing::Size(100, 50);
			this->zBox->TabIndex = 1;
			this->zBox->TabStop = false;
			// 
			// scBtn
			// 
			this->scBtn->Location = System::Drawing::Point(70, 50);
			this->scBtn->Name = L"scBtn";
			this->scBtn->Size = System::Drawing::Size(75, 23);
			this->scBtn->TabIndex = 2;
			this->scBtn->Text = L"Screenshot!";
			this->scBtn->UseVisualStyleBackColor = true;
			// 
			// statusLab
			// 
			this->statusLab->AutoSize = true;
			this->statusLab->Location = System::Drawing::Point(0, 50);
			this->statusLab->Name = L"statusLab";
			this->statusLab->Size = System::Drawing::Size(70, 25);
			this->statusLab->TabIndex = 3;
			this->statusLab->Text = L"label1";
			// 
			// PreviewWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(12, 25);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(274, 229);
			this->Controls->Add(this->statusLab);
			this->Controls->Add(this->scBtn);
			this->Controls->Add(this->zBox);
			this->Controls->Add(this->bgrBox);
			this->MaximizeBox = false;
			this->Name = L"PreviewWindow";
			this->Text = L"PreviewWindow";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bgrBox))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->zBox))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}
