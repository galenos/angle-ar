#pragma once
#include "PreviewWindow.h"
#include "KinectSource.h"
#include "BT2000Source.h"
#include "ObjectDetect.h"

using namespace Backend;

ref class Globals
{
public:

	static property ObjectDetect ^Detect;
	static property PreviewWindow ^Preview;
	static property IBgrzSource ^Source;
};
