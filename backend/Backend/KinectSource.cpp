#include "stdafx.h"
#include "KinectSource.h"

using namespace System::Runtime::InteropServices;

KinectSource::KinectSource(int rangeMin, int rangeMax) :
	depthMin(rangeMin), depthMax(rangeMax), depthD(rangeMax - rangeMin)
{
	sensor = KinectSensor::GetDefault();
	sensor->Open();

	frameReader = sensor->OpenMultiSourceFrameReader(
		FrameSourceTypes::Color |
		FrameSourceTypes::Depth);
	frameReader->MultiSourceFrameArrived +=
		gcnew EventHandler<MultiSourceFrameArrivedEventArgs ^>(
			this, &KinectSource::ProcessFrame);
}

void KinectSource::BeginAcquireFrame()
{
	frameRequested = true;
}

void KinectSource::ProcessFrame(Object ^sender, MultiSourceFrameArrivedEventArgs ^e)
{
	if (!frameRequested) return;

	MultiSourceFrame ^msFrame = e->FrameReference->AcquireFrame();
	ColorFrame ^cFrame;
	DepthFrame ^zFrame;

	static cv::Mat bgrMat = cv::Mat();
	static cv::Mat zMat = cv::Mat();

	if (bgrMat.empty() && (cFrame = msFrame->ColorFrameReference->AcquireFrame()) != nullptr)
	{
		FrameDescription ^cDes = cFrame->FrameDescription;

		array<byte> ^bgraDat = gcnew array<byte>(cDes->Width * cDes->Height * 4);
		bgrMat = cv::Mat(cDes->Height, cDes->Width, CV_8UC4);

		cFrame->CopyConvertedFrameDataToArray(bgraDat, ColorImageFormat::Bgra);
		Marshal::Copy(bgraDat, 0, IntPtr(bgrMat.data), bgraDat->Length);

		cv::cvtColor(bgrMat, bgrMat, cv::ColorConversionCodes::COLOR_BGRA2BGR);

		delete cFrame;
	}

	if (zMat.empty() && (zFrame = msFrame->DepthFrameReference->AcquireFrame()) != nullptr)
	{
		FrameDescription ^zDes = zFrame->FrameDescription;

		array<ushort> ^zDatShort = gcnew array<ushort>(zDes->Width * zDes->Height);
		array<byte> ^zDat = gcnew array<byte>(zDatShort->Length * sizeof(ushort));
		zMat = cv::Mat(zDes->Height, zDes->Width, CV_16UC1);

		zFrame->CopyFrameDataToArray(zDatShort);
		Buffer::BlockCopy(zDatShort, 0, zDat, 0, zDat->Length);
		Marshal::Copy(zDat, 0, IntPtr(zMat.data), zDat->Length);

		uchar *data = zMat.data;
		for (int i = 0; i < zMat.rows; ++i) {
			ushort *row = (ushort *) &data[zMat.step * i];
			for (int j = 0; j < zMat.cols; ++j) {
				if (row[j] <= depthMin) row[j] = 0;
				else if (row[j] >= depthMax) row[j] = depthD;
				else row[j] -= depthMin;
			}
		}

		zMat.convertTo(zMat, CV_8UC1, 255.0 / depthD);

		delete zFrame;
	}

	if (!bgrMat.empty() && !zMat.empty())
	{
		frameRequested = false;
		FrameArrived(gcnew BgrzFrame(bgrMat, zMat));
		bgrMat = cv::Mat();
		zMat = cv::Mat();
	}
}
