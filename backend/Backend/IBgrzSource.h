#pragma once
#include "BgrzFrame.h"

delegate void FrameHandler(BgrzFrame ^frame);

interface class IBgrzSource
{
public:

	event FrameHandler ^FrameArrived;
	void BeginAcquireFrame();
};
