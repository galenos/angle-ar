//------------------------------------------------------------------------------
// <copyright file="DepthBasics.cpp" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

#include "stdafx.h"
#include <strsafe.h>
#include "resource.h"
#include "MultiBasics.h"
#include <Shlwapi.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>

using namespace cv;
using namespace std;

Mat OpenCVGarbage_depth = cvCreateImage(cvSize(512, 424), 8, 1);
Mat OpenCVGarbage_color = cvCreateImage(cvSize(1920, 1080), 8, 4);
float color_UL = -42;
float color_UR = -42;
float color_LL = -42;
float color_LR = -42;
float depth_UL = -24;
float depth_UR = -24;
float depth_LL = -24;
float depth_LR = -24;

/// <summary>
/// Entry point for the application
/// </summary>
/// <param name="hInstance">handle to the application instance</param>
/// <param name="hPrevInstance">always 0</param>
/// <param name="lpCmdLine">command line arguments</param>
/// <param name="nCmdShow">whether to display minimized, maximized, or normally</param>
/// <returns>status</returns>
int APIENTRY wWinMain(
	_In_ HINSTANCE hInstance,
    _In_opt_ HINSTANCE hPrevInstance,
    _In_ LPWSTR lpCmdLine,
    _In_ int nShowCmd
)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    CMultiBasics application;
    application.Run(hInstance, nShowCmd);
}

/// <summary>
/// Constructor
/// </summary>
CMultiBasics::CMultiBasics() :
    m_hWnd(NULL),
    m_nStartTime(0),
    m_nLastCounter(0),
    m_nFramesSinceUpdate(0),
    m_fFreq(0),
    m_nNextStatusTime(0LL),
    m_bSaveScreenshot(false),
    m_pKinectSensor(NULL),
    m_pMultiFrameReader(NULL),
    m_pD2DFactory(NULL),
    m_pDrawDepth(NULL),
	//m_pDrawColor(NULL),
    m_pDepthRGBX(NULL),
	m_pColorRGBX(NULL)
{
    LARGE_INTEGER qpf = {0};
    if (QueryPerformanceFrequency(&qpf))
    {
        m_fFreq = double(qpf.QuadPart);
    }

    // create heap storage for depth pixel data in RGBX format
    m_pDepthRGBX = new RGBQUAD[cDepthWidth * cDepthHeight];
	m_pColorRGBX = new RGBQUAD[cColorWidth * cColorHeight];
}
  

/// <summary>
/// Destructor
/// </summary>
CMultiBasics::~CMultiBasics()
{
    // clean up Direct2D renderer
    if (m_pDrawDepth)
    {
        delete m_pDrawDepth;
        m_pDrawDepth = NULL;
    }

	/*
	if (m_pDrawColor)
	{
		delete m_pDrawColor;
		m_pDrawColor = NULL;
	}
	*/

	if (m_pColorRGBX)
	{
		delete[] m_pColorRGBX;
		m_pColorRGBX = NULL;
	}

    if (m_pDepthRGBX)
    {
        delete [] m_pDepthRGBX;
        m_pDepthRGBX = NULL;
    }

    // clean up Direct2D
    SafeRelease(m_pD2DFactory);

    // done with depth frame reader
    SafeRelease(m_pMultiFrameReader);

    // close the Kinect Sensor
    if (m_pKinectSensor)
    {
        m_pKinectSensor->Close();
    }

    SafeRelease(m_pKinectSensor);
}

/// <summary>
/// Creates the main window and begins processing
/// </summary>
/// <param name="hInstance">handle to the application instance</param>
/// <param name="nCmdShow">whether to display minimized, maximized, or normally</param>
int CMultiBasics::Run(HINSTANCE hInstance, int nCmdShow)
{
    MSG       msg = {0};
    WNDCLASS  wc;

    // Dialog custom window class
    ZeroMemory(&wc, sizeof(wc));
    wc.style         = CS_HREDRAW | CS_VREDRAW;
    wc.cbWndExtra    = DLGWINDOWEXTRA;
    wc.hCursor       = LoadCursorW(NULL, IDC_ARROW);
    wc.hIcon         = LoadIconW(hInstance, MAKEINTRESOURCE(IDI_APP));
    wc.lpfnWndProc   = DefDlgProcW;
    wc.lpszClassName = L"DepthBasicsAppDlgWndClass";

    if (!RegisterClassW(&wc))
    {
        return 0;
    }

    // Create main application window
    HWND hWndApp = CreateDialogParamW(
        NULL,
        MAKEINTRESOURCE(IDD_APP),
        NULL,
        (DLGPROC)CMultiBasics::MessageRouter, 
        reinterpret_cast<LPARAM>(this));

    // Show window
    ShowWindow(hWndApp, nCmdShow);

    // Main message loop
    while (WM_QUIT != msg.message)
    {
        Update();

        while (PeekMessageW(&msg, NULL, 0, 0, PM_REMOVE))
        {
            // If a dialog message will be taken care of by the dialog proc
            if (hWndApp && IsDialogMessageW(hWndApp, &msg))
            {
                continue;
            }

            TranslateMessage(&msg);
            DispatchMessageW(&msg);
        }
    }

    return static_cast<int>(msg.wParam);
}

/// <summary>
/// Main processing function
/// </summary>
void CMultiBasics::Update()
{
    if (!m_pMultiFrameReader)
    {
        return;
    }

	IMultiSourceFrame* pMultiFrame = NULL;
	HRESULT hr = m_pMultiFrameReader->AcquireLatestFrame(&pMultiFrame);

	//Depth Processing
	if (SUCCEEDED(hr))
	{
		IDepthFrame* pDepthFrame = NULL;
		IDepthFrameReference* pDepthFrameRef = NULL;
		hr = pMultiFrame->get_DepthFrameReference(&pDepthFrameRef);
		pDepthFrameRef->AcquireFrame(&pDepthFrame);
		SafeRelease(pDepthFrameRef);

		if (SUCCEEDED(hr))
		{
			INT64 nTime = 0;
			IFrameDescription* pFrameDescription = NULL;
			int nWidth = 0;
			int nHeight = 0;
			USHORT nDepthMinReliableDistance = 0;
			USHORT nDepthMaxDistance = 0;
			UINT nBufferSize = 0;
			UINT16 *pBuffer = NULL;

			if (pDepthFrame != NULL && pDepthFrame != nullptr)
			{

				hr = pDepthFrame->get_RelativeTime(&nTime);

				if (SUCCEEDED(hr))
				{
					hr = pDepthFrame->get_FrameDescription(&pFrameDescription);
				}

				if (SUCCEEDED(hr))
				{
					hr = pFrameDescription->get_Width(&nWidth);
				}

				if (SUCCEEDED(hr))
				{
					hr = pFrameDescription->get_Height(&nHeight);
				}

				if (SUCCEEDED(hr))
				{
					hr = pDepthFrame->get_DepthMinReliableDistance(&nDepthMinReliableDistance);
				}

				if (SUCCEEDED(hr))
				{
					// In order to see the full range of depth (including the less reliable far field depth)
					// we are setting nDepthMaxDistance to the extreme potential depth threshold
					nDepthMaxDistance = USHRT_MAX;

					// Note:  If you wish to filter by reliable depth distance, uncomment the following line.
					//// hr = pDepthFrame->get_DepthMaxReliableDistance(&nDepthMaxDistance);
				}

				if (SUCCEEDED(hr))
				{
					hr = pDepthFrame->AccessUnderlyingBuffer(&nBufferSize, &pBuffer);
				}

				if (SUCCEEDED(hr))
				{
					ProcessDepth(nTime, pBuffer, nWidth, nHeight, nDepthMinReliableDistance, nDepthMaxDistance);
				}
			}
			SafeRelease(pFrameDescription);
		}

		SafeRelease(pDepthFrame);

		//Color Processing
		IColorFrame* pColorFrame = NULL;
		IColorFrameReference* pColorFrameRef = NULL;
		hr = pMultiFrame->get_ColorFrameReference(&pColorFrameRef);
		pColorFrameRef->AcquireFrame(&pColorFrame);
		SafeRelease(pColorFrameRef);

		if (SUCCEEDED(hr))
		{
			INT64 nTime = 0;
			IFrameDescription* pFrameDescription = NULL;
			int nWidth = 0;
			int nHeight = 0;
			ColorImageFormat imageFormat = ColorImageFormat_None;
			UINT nBufferSize = 0;
			RGBQUAD *pBuffer = NULL;

			if (pColorFrame != NULL && pColorFrame != nullptr) {

				hr = pColorFrame->get_RelativeTime(&nTime);

				if (SUCCEEDED(hr))
				{
					hr = pColorFrame->get_FrameDescription(&pFrameDescription);
				}

				if (SUCCEEDED(hr))
				{
					hr = pFrameDescription->get_Width(&nWidth);
				}

				if (SUCCEEDED(hr))
				{
					hr = pFrameDescription->get_Height(&nHeight);
				}

				if (SUCCEEDED(hr))
				{
					hr = pColorFrame->get_RawColorImageFormat(&imageFormat);
				}

				if (SUCCEEDED(hr))
				{
					if (imageFormat == ColorImageFormat_Bgra)
					{
						hr = pColorFrame->AccessRawUnderlyingBuffer(&nBufferSize, reinterpret_cast<BYTE**>(&pBuffer));
					}
					else if (m_pColorRGBX)
					{
						pBuffer = m_pColorRGBX;
						OpenCVGarbage_color(cColorHeight, cColorWidth, CV_8UC3, reinterpret_cast<void*>(pBuffer));
						Mat UL_color = Mat(OpenCVGarbage_Color, Rect(720, 220, 290, 290));
						Mat UR_color = Mat(OpenCVGarbage_Color, Rect(720, 220, 290, 290));
						Mat LL_color = Mat(OpenCVGarbage_Color, Rect(1010, 510, 290, 290));
						Mat LR_color = Mat(OpenCVGarbage_Color, Rect(1010, 510, 290, 290));

						Mat UL_c1d(1, 290 * 290, CV_32FC1);
						Mat UR_c1d(1, 290 * 290, CV_32FC1);
						Mat LL_c1d(1, 290 * 290, CV_32FC1);
						Mat LR_c1d(1, 290 * 290, CV_32FC1);

						int i = 0;
						int j = 0;
						for (i = 0; i < UL_color.rows; i++)
						{
							for (j = 0; j < UL_color.cols; j++)
							{
								UL_c1d.at<float>(1, ii++) = UL_color.at<uchar>(i, j);
							}
						}
						for (i = 0; i < UR_color.rows; i++)
						{
							for (j = 0; j < UR_color.cols; j++)
							{
								UR_c1d.at<float>(1, ii++) = UR_color.at<uchar>(i, j);
							}
						}
						for (i = 0; i < LL_color.rows; i++)
						{
							for (j = 0; j < LL_color.cols; j++)
							{
								LL_c1d.at<float>(1, ii++) = LL_color.at<uchar>(i, j);
							}
						}
						for (i = 0; i < LR_color.rows; i++)
						{
							for (j = 0; j < LR_color.cols; j++)
							{
								LR_c1d.at<float>(1, ii++) = LR_color.at<uchar>(i, j);
							}
						}

						nBufferSize = cColorWidth * cColorHeight * sizeof(RGBQUAD);
						hr = pColorFrame->CopyConvertedFrameDataToArray(nBufferSize, reinterpret_cast<BYTE*>(pBuffer), ColorImageFormat_Bgra);
					}
					else
					{
						hr = E_FAIL;
					}
				}

				if (SUCCEEDED(hr))
				{
					ProcessColor(nTime, pBuffer, nWidth, nHeight);
				}

				SafeRelease(pFrameDescription);
			}
		}

		SafeRelease(pColorFrame);
	}
	cvSVM svm_color;
	svm_color.open("Color_Training");
	CvSVM svm_depth;
	svm_depth.open("Depth_Training");

	color_UL = svm_color.predict(UL_c1d);
	color_UR = svm_color.predict(UR_c1d);
	color_LL = svm_color.predict(LL_c1d);
	color_LR = svm_color.predict(LR_c1d);
	depth_UL = svm_depth.predict(UL_d1d);
	depth_UR = svm_depth.predict(UR_d1d);
	depth_LL = svm_depth.predict(LL_d1d);
	depth_LR = svm_depth.predict(LR_d1d);

	WCHAR szStatusMessage[64 + MAX_PATH];

	if (color_UL != -42)
	{
		if (depth_UL == color_UL)
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Category %f. color and depth", color_UL);
		}
		else
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Color result %f != depth result %f", color_UL, depth_UL);
		}
		SetStatusMessage(szStatusMessage, 5000, true);
	}
	else if (color_UR != -42)
	{
		if (depth_UR == color_UR)
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Category %f. color and depth",color_UR);
		}
		else
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Color result %f != depth result %f",color_UR,depth_UR);
		}
		SetStatusMessage(szStatusMessage, 5000, true);
	}
	else if (color_LL != -42)
	{
		if (depth_LL == color_LL)
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Category %f. color and depth", color_LL);
		}
		else
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Color result %f != depth result %f", color_LL, depth_LL);
		}
		SetStatusMessage(szStatusMessage, 5000, true);
	}
	else if (color_LR != -42)
	{
		if (depth_LR == color_LR)
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Category %f. color and depth",color_LR);
		}
		else
		{
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Color result %f != depth result %f", color_LR, depth_LR);
		}
		SetStatusMessage(szStatusMessage, 5000, true);
	}
	else
	{
		StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"UL_C %f, UL_D %f, UR_C %f, UR_D %f, LL_C %f, LL_D %f, LR_C %f, LR_D %f", color_UL, depth_UL,color_UR,depth_UR,color_LL,depth_LL,color_LR,depth_LR);
		SetStatusMessage(szStatusMessage, 5000, true);
	}

	SafeRelease(pMultiFrame);
}

/// <summary>
/// Handles window messages, passes most to the class instance to handle
/// </summary>
/// <param name="hWnd">window message is for</param>
/// <param name="uMsg">message</param>
/// <param name="wParam">message data</param>
/// <param name="lParam">additional message data</param>
/// <returns>result of message processing</returns>
LRESULT CALLBACK CMultiBasics::MessageRouter(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    CMultiBasics* pThis = NULL;
    
    if (WM_INITDIALOG == uMsg)
    {
        pThis = reinterpret_cast<CMultiBasics*>(lParam);
        SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pThis));
    }
    else
    {
        pThis = reinterpret_cast<CMultiBasics*>(::GetWindowLongPtr(hWnd, GWLP_USERDATA));
    }

    if (pThis)
    {
        return pThis->DlgProc(hWnd, uMsg, wParam, lParam);
    }

    return 0;
}

/// <summary>
/// Handle windows messages for the class instance
/// </summary>
/// <param name="hWnd">window message is for</param>
/// <param name="uMsg">message</param>
/// <param name="wParam">message data</param>
/// <param name="lParam">additional message data</param>
/// <returns>result of message processing</returns>
LRESULT CALLBACK CMultiBasics::DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(wParam);
    UNREFERENCED_PARAMETER(lParam);

    switch (message)
    {
        case WM_INITDIALOG:
        {
            // Bind application window handle
            m_hWnd = hWnd;

            // Init Direct2D
            D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pD2DFactory);

            // Create and initialize a new Direct2D image renderer (take a look at ImageRenderer.h)
            // We'll use this to draw the data we receive from the Kinect to the screen
            m_pDrawDepth = new ImageRenderer();

			HRESULT hr = NULL;

			hr = m_pDrawDepth->Initialize(GetDlgItem(m_hWnd, IDC_VIDEOVIEW), m_pD2DFactory, cDepthWidth, cDepthHeight, cDepthWidth * sizeof(RGBQUAD));
			//hr = m_pDrawColor->Initialize(GetDlgItem(m_hWnd, IDC_VIDEOVIEW), m_pD2DFactory, cColorWidth, cColorHeight, cColorWidth * sizeof(RGBQUAD));

            if (FAILED(hr))
            {
                SetStatusMessage(L"Failed to initialize the Direct2D draw device.", 10000, true);
            }

            // Get and initialize the default Kinect sensor
            InitializeDefaultSensor();
        }
        break;

        // If the titlebar X is clicked, destroy app
        case WM_CLOSE:
            DestroyWindow(hWnd);
            break;

        case WM_DESTROY:
            // Quit the main message pump
            PostQuitMessage(0);
            break;

        // Handle button press
        case WM_COMMAND:
            // If it was for the screenshot control and a button clicked event, save a screenshot next frame 
            if (IDC_BUTTON_SCREENSHOT == LOWORD(wParam) && BN_CLICKED == HIWORD(wParam))
            {
                m_bSaveScreenshot = true;
            }
            break;
    }

    return FALSE;
}

/// <summary>
/// Initializes the default Kinect sensor
/// </summary>
/// <returns>indicates success or failure</returns>
HRESULT CMultiBasics::InitializeDefaultSensor()
{
    HRESULT hr;

    hr = GetDefaultKinectSensor(&m_pKinectSensor);
    if (FAILED(hr))
    {
        return hr;
    }

    if (m_pKinectSensor)
    {
        // Initialize the Kinect and get frame reader for Depth and Color

        hr = m_pKinectSensor->Open();
		hr = m_pKinectSensor->OpenMultiSourceFrameReader(FrameSourceTypes::FrameSourceTypes_Depth | FrameSourceTypes::FrameSourceTypes_Color, &m_pMultiFrameReader);
    }

    if (!m_pKinectSensor || FAILED(hr))
    {
        SetStatusMessage(L"No ready Kinect found!", 10000, true);
        return E_FAIL;
    }

    return hr;
}

/// <summary>
/// Handle new depth data
/// <param name="nTime">timestamp of frame</param>
/// <param name="pBuffer">pointer to frame data</param>
/// <param name="nWidth">width (in pixels) of input image data</param>
/// <param name="nHeight">height (in pixels) of input image data</param>
/// <param name="nMinDepth">minimum reliable depth</param>
/// <param name="nMaxDepth">maximum reliable depth</param>
/// </summary>
void CMultiBasics::ProcessDepth(INT64 nTime, const UINT16* pBuffer, int nWidth, int nHeight, USHORT nMinDepth, USHORT nMaxDepth)
{
	if (m_hWnd)
	{
		if (!m_nStartTime)
		{
			m_nStartTime = nTime;
		}

		double fps = 0.0;

		LARGE_INTEGER qpcNow = { 0 };
		if (m_fFreq)
		{
			if (QueryPerformanceCounter(&qpcNow))
			{
				if (m_nLastCounter)
				{
					m_nFramesSinceUpdate++;
					fps = m_fFreq * m_nFramesSinceUpdate / double(qpcNow.QuadPart - m_nLastCounter);
				}
			}
		}

		WCHAR szStatusMessage[64];
		StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L" FPS = %0.2f    Time = %I64d", fps, (nTime - m_nStartTime));

		if (SetStatusMessage(szStatusMessage, 1000, false))
		{
			m_nLastCounter = qpcNow.QuadPart;
			m_nFramesSinceUpdate = 0;
		}
	}

	// Make sure we've received valid data
	if (m_pDepthRGBX && pBuffer && (nWidth == cDepthWidth) && (nHeight == cDepthHeight))
	{
		RGBQUAD* pRGBX = m_pDepthRGBX;

		// end pixel is start + width*height - 1
		const UINT16* pBufferEnd = pBuffer + (nWidth * nHeight);

		while (pBuffer < pBufferEnd)
		{
			USHORT depth = *pBuffer;

			// To convert to a byte, we're discarding the most-significant
			// rather than least-significant bits.
			// We're preserving detail, although the intensity will "wrap."
			// Values outside the reliable depth range are mapped to 0 (black).

			// Note: Using conditionals in this loop could degrade performance.
			// Consider using a lookup table instead when writing production code.
			BYTE intensity = static_cast<BYTE>((depth >= nMinDepth) && (depth <= nMaxDepth) ? (depth % 256) : 0);

			pRGBX->rgbRed   = intensity;
			pRGBX->rgbGreen = intensity;
			pRGBX->rgbBlue  = intensity;
			++pRGBX;
			++pBuffer;
			//(reinterpret_cast<void*>(pRGBX), OpenCVGarbage_depth, 1 / 16, 0);
			OpenCVGarbage_depth(cDepthHeight, cDepthWidth, CV_8UC1, reinterpret_cast<void*>(pRGBX));
			Mat UL_depth = Mat(OpenCVGarbage_depth, Rect(200, 100, 90, 90));
			Mat UR_depth = Mat(OpenCVGarbage_depth, Rect(290, 100, 90, 90));
			Mat LL_depth = Mat(OpenCVGarbage_depth, Rect(200, 190, 90, 90));
			Mat LR_depth = Mat(OpenCVGarbage_depth, Rect(290, 190, 90, 90));

			Mat UL_d1d(1, 90 * 90, CV_32FC1);
			Mat UR_d1d(1, 90 * 90, CV_32FC1);
			Mat LL_d1d(1, 90 * 90, CV_32FC1);
			Mat LR_d1d(1, 90 * 90, CV_32FC1);

			int i = 0;
			int j = 0;
			for (i = 0; i < UL_depth.rows; i++)
			{
				for (j = 0; j < UL_depth.cols; j++)
				{
					UL_d1d.at<float>(1, ii++) = UL_depth.at<uchar>(i, j);
				}
			}
			for (i = 0; i < UR_depth.rows; i++)
			{
				for (j = 0; j < UR_depth.cols; j++)
				{
					UR_d1d.at<float>(1, ii++) = UR_depth.at<uchar>(i, j);
				}
			}
			for (i = 0; i < LL_depth.rows; i++)
			{
				for (j = 0; j < LL_depth.cols; j++)
				{
					LL_d1d.at<float>(1, ii++) = LL_depth.at<uchar>(i, j);
				}
			}
			for (i = 0; i < LR_color.rows; i++)
			{
				for (j = 0; j < LR_color.cols; j++)
				{
					LR_d1d.at<float>(1, ii++) = LR_depth.at<uchar>(i, j);
				}
			}
		}

		// Draw the data with Direct2D
		m_pDrawDepth->Draw(reinterpret_cast<BYTE*>(m_pDepthRGBX), cDepthWidth * cDepthHeight * sizeof(RGBQUAD));

		if (m_bSaveScreenshot)
		{
			WCHAR szScreenshotPath[MAX_PATH];

			// Retrieve the path to My Photos
			GetScreenshotFileName(szScreenshotPath, _countof(szScreenshotPath));

			// Write out the bitmap to disk
			HRESULT hr = SaveBitmapToFile(reinterpret_cast<BYTE*>(m_pDepthRGBX), nWidth, nHeight, sizeof(RGBQUAD) * 8, szScreenshotPath);

			WCHAR szStatusMessage[64 + MAX_PATH];
			if (SUCCEEDED(hr))
			{
				// Set the status bar to show where the screenshot was saved
				StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Screenshot saved to %s", szScreenshotPath);
			}
			else
			{
				StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Failed to write screenshot to %s", szScreenshotPath);
			}

			SetStatusMessage(szStatusMessage, 5000, true);

			// toggle off so we don't save a screenshot again next frame
			m_bSaveScreenshot = false;
		}
	}
}

/// <summary>
/// Handle new color data
/// <param name="nTime">timestamp of frame</param>
/// <param name="pBuffer">pointer to frame data</param>
/// <param name="nWidth">width (in pixels) of input image data</param>
/// <param name="nHeight">height (in pixels) of input image data</param>
/// </summary>
void CMultiBasics::ProcessColor(INT64 nTime, RGBQUAD* pBuffer, int nWidth, int nHeight)
{
	/*
	if(!dataFlag) {
		if (m_hWnd)
		{
			if (!m_nStartTime)
			{
				m_nStartTime = nTime;
			}

			double fps = 0.0;

			LARGE_INTEGER qpcNow = { 0 };
			if (m_fFreq)
			{
				if (QueryPerformanceCounter(&qpcNow))
				{
					if (m_nLastCounter)
					{
						m_nFramesSinceUpdate++;
						fps = m_fFreq * m_nFramesSinceUpdate / double(qpcNow.QuadPart - m_nLastCounter);
					}
				}
			}

			WCHAR szStatusMessage[64];
			StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L" FPS = %0.2f    Time = %I64d", fps, (nTime - m_nStartTime));

			if (SetStatusMessage(szStatusMessage, 1000, false))
			{
				m_nLastCounter = qpcNow.QuadPart;
				m_nFramesSinceUpdate = 0;
			}
		}
		*/
		// Make sure we've received valid data
		if (pBuffer && (nWidth == cColorWidth) && (nHeight == cColorHeight))
		{
			/*
			// Draw the data with Direct2D
			m_pDrawColor->Draw(reinterpret_cast<BYTE*>(pBuffer), cColorWidth * cColorHeight * sizeof(RGBQUAD));

			if (m_bSaveScreenshot)
			{
				WCHAR szScreenshotPath[MAX_PATH];

				// Retrieve the path to My Photos
				GetScreenshotFileName(szScreenshotPath, _countof(szScreenshotPath));

				// Write out the bitmap to disk
				HRESULT hr = SaveBitmapToFile(reinterpret_cast<BYTE*>(pBuffer), nWidth, nHeight, sizeof(RGBQUAD) * 8, szScreenshotPath);

				WCHAR szStatusMessage[64 + MAX_PATH];
				if (SUCCEEDED(hr))
				{
					// Set the status bar to show where the screenshot was saved
					StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Screenshot saved to %s", szScreenshotPath);
				}
				else
				{
					StringCchPrintf(szStatusMessage, _countof(szStatusMessage), L"Failed to write screenshot to %s", szScreenshotPath);
				}

				SetStatusMessage(szStatusMessage, 5000, true);

				// toggle off so we don't save a screenshot again next frame
				m_bSaveScreenshot = false;
				*/
			//}
		}
	//}
}

/// <summary>
/// Set the status bar message
/// </summary>
/// <param name="szMessage">message to display</param>
/// <param name="showTimeMsec">time in milliseconds to ignore future status messages</param>
/// <param name="bForce">force status update</param>
bool CMultiBasics::SetStatusMessage(_In_z_ WCHAR* szMessage, DWORD nShowTimeMsec, bool bForce)
{
    INT64 now = GetTickCount64();

    if (m_hWnd && (bForce || (m_nNextStatusTime <= now)))
    {
        SetDlgItemText(m_hWnd, IDC_STATUS, szMessage);
        m_nNextStatusTime = now + nShowTimeMsec;

        return true;
    }

    return false;
}

/// <summary>
/// Get the name of the file where screenshot will be stored.
/// </summary>
/// <param name="lpszFilePath">string buffer that will receive screenshot file name.</param>
/// <param name="nFilePathSize">number of characters in lpszFilePath string buffer.</param>
/// <returns>
/// S_OK on success, otherwise failure code.
/// </returns>
HRESULT CMultiBasics::GetScreenshotFileName(_Out_writes_z_(nFilePathSize) LPWSTR lpszFilePath, UINT nFilePathSize)
{
    WCHAR* pszKnownPath = NULL;
    HRESULT hr = SHGetKnownFolderPath(FOLDERID_Pictures, 0, NULL, &pszKnownPath);

    if (SUCCEEDED(hr))
    {
        // Get the time
        WCHAR szTimeString[MAX_PATH];
        GetTimeFormatEx(NULL, 0, NULL, L"hh'-'mm'-'ss", szTimeString, _countof(szTimeString));

        // File name will be KinectScreenshotDepth-HH-MM-SS.bmp
        StringCchPrintfW(lpszFilePath, nFilePathSize, L"%s\\KinectScreenshot-Depth-%s.bmp", pszKnownPath, szTimeString);
    }

    if (pszKnownPath)
    {
        CoTaskMemFree(pszKnownPath);
    }

    return hr;
}

/// <summary>
/// Save passed in image data to disk as a bitmap
/// </summary>
/// <param name="pBitmapBits">image data to save</param>
/// <param name="lWidth">width (in pixels) of input image data</param>
/// <param name="lHeight">height (in pixels) of input image data</param>
/// <param name="wBitsPerPixel">bits per pixel of image data</param>
/// <param name="lpszFilePath">full file path to output bitmap to</param>
/// <returns>indicates success or failure</returns>
HRESULT CMultiBasics::SaveBitmapToFile(BYTE* pBitmapBits, LONG lWidth, LONG lHeight, WORD wBitsPerPixel, LPCWSTR lpszFilePath)
{
    DWORD dwByteCount = lWidth * lHeight * (wBitsPerPixel / 8);

    BITMAPINFOHEADER bmpInfoHeader = {0};

    bmpInfoHeader.biSize        = sizeof(BITMAPINFOHEADER);  // Size of the header
    bmpInfoHeader.biBitCount    = wBitsPerPixel;             // Bit count
    bmpInfoHeader.biCompression = BI_RGB;                    // Standard RGB, no compression
    bmpInfoHeader.biWidth       = lWidth;                    // Width in pixels
    bmpInfoHeader.biHeight      = -lHeight;                  // Height in pixels, negative indicates it's stored right-side-up
    bmpInfoHeader.biPlanes      = 1;                         // Default
    bmpInfoHeader.biSizeImage   = dwByteCount;               // Image size in bytes

    BITMAPFILEHEADER bfh = {0};

    bfh.bfType    = 0x4D42;                                           // 'M''B', indicates bitmap
    bfh.bfOffBits = bmpInfoHeader.biSize + sizeof(BITMAPFILEHEADER);  // Offset to the start of pixel data
    bfh.bfSize    = bfh.bfOffBits + bmpInfoHeader.biSizeImage;        // Size of image + headers

    // Create the file on disk to write to
    HANDLE hFile = CreateFileW(lpszFilePath, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

    // Return if error opening file
    if (NULL == hFile) 
    {
        return E_ACCESSDENIED;
    }

    DWORD dwBytesWritten = 0;
    
    // Write the bitmap file header
    if (!WriteFile(hFile, &bfh, sizeof(bfh), &dwBytesWritten, NULL))
    {
        CloseHandle(hFile);
        return E_FAIL;
    }
    
    // Write the bitmap info header
    if (!WriteFile(hFile, &bmpInfoHeader, sizeof(bmpInfoHeader), &dwBytesWritten, NULL))
    {
        CloseHandle(hFile);
        return E_FAIL;
    }
    
    // Write the RGB Data
    if (!WriteFile(hFile, pBitmapBits, bmpInfoHeader.biSizeImage, &dwBytesWritten, NULL))
    {
        CloseHandle(hFile);
        return E_FAIL;
    }    

    // Close the file
    CloseHandle(hFile);
    return S_OK;
}
