#include "Header.h"
#include <Shlwapi.h>
using namespace cv;
using namespace std;



int main()
{

	//From Kinect DEPTH
	/*
	RGBAQUAD kinectGarbage_depth = new RGBAQuad(height*width);
	Mat OpenCVGarbage_depth(height, width, CV_8UC4, reinterpret_cast<void*>(kinectGarbage));
	//Crop out trash from background.  Keeping only Vertical: 100-280
	//												Horizontal:200-380
	//Break into 4 equal chunks 90 x 90 (for depth)
	Mat UL_depth = Mat(OpenCVGarbage, Rect(200,100,90,90));
	Mat UR_depth = Mat(OpenCVGarbage, Rect(290,100,90,90));
	
	Mat LL_depth = Mat(OpenCVGarbage, Rect(200,190,90,90));
	Mat LR_depth = Mat(OpenCVGarbage, Rect(290,190,90,90));
	
	*/
	
	//From Kinect COLOR
	/*
	RGBAQUAD kinectGarbage_color = new RGBAQuad(height*width);
	Mat OpenCVGarbage_color(height, width, CV_8UC4, reinterpret_cast<void*>(kinectGarbage));
	//Crop out trash from background.  Keeping only Vertical: 220-800 (580)
	//												Horizontal:720-1300 (580)
	//Break into 4 equal chunks 90 x 90 (for depth)
	Mat UL_color = Mat(OpenCVGarbage, Rect(720,220,290,290));
	Mat UR_color = Mat(OpenCVGarbage, Rect(720,220,290,290));
	
	Mat LL_color = Mat(OpenCVGarbage, Rect(1010,510,290,290));
	Mat LR_color = Mat(OpenCVGarbage, Rect(1010,510,290,290));
	
	*/
	//string filenames[1000];//limit of 1000 images per dir
	cout << "Listing filenames\n\n";
	string filenames[24]=
	{ 
	"c0_000.jpg", "c0_001.jpg", "c0_002.jpg", "c0_003.jpg", "c0_004.jpg",
	"c0_005.jpg", "c0_006.jpg", "c0_007.jpg",
	"c1_000.jpg", "c1_001.jpg", "c1_002.jpg", "c1_003.jpg", "c1_004.jpg",
	"c1_005.jpg", "c0_001.jpg", "c1_007.jpg",
	"c2_000.jpg", "c2_001.jpg", "c2_002.jpg", "c2_003.jpg", "c2_004.jpg",
	"c2_005.jpg", "c2_001.jpg", "c2_007.jpg"
	};

	int n = 24; 
	//size_t size;
	//size_t *ret;
	//size_t max = 260 * sizeof(char);
	//char buff[MAX_PATH];

	//WCHAR wildcard_path[MAX_PATH];
	//WIN32_FIND_DATA ffd;
	//WCHAR file_path[MAX_PATH];
	//wchar_t *c0 = L"class0";
	//wchar_t *c1 = L"class1";
	//wchar_t *c2 = L"class2";
	//wchar_t *star = L"*";
	/*
	//Figure out how many images we have to deal with for class 0
	PathCombine(wildcard_path, c0, star);
	//PathCchCombine(wildcard_path, MAX_PATH, c0, star);//change as appropriate
	HANDLE hFind = FindFirstFile(wildcard_path, &ffd);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		//This is not good.
	}
	do{
		PathCombine(file_path, c0, ffd.cFileName);
		//PathCchCombine(file_path, MAX_PATH, c0, ffd.cFileName);
		size = (wcslen(file_path + 1) * sizeof(wchar_t));
		wcstombs_s(&size, buff, max, file_path, max);
		//wcstombs(&buff[0], file_path, size);
		filenames[n] = buff;
		n++;
	} while (FindNextFile(hFind, &ffd) != 0);
	FindClose(hFind);

	//Figure out how many images we have to deal with for class 1
	PathCombine(wildcard_path, c1, star);
	//PathCchCombine(wildcard_path, MAX_PATH, c1, star);//change as appropriate
	hFind = FindFirstFile(wildcard_path, &ffd);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		//This is not good.
	}
	do{
		PathCombine(file_path, c1, ffd.cFileName);
		//PathCchCombine(file_path, MAX_PATH, c1, ffd.cFileName);
		size = (wcslen(file_path + 1) * sizeof(wchar_t));
		wcstombs_s(&size, buff, max, file_path, max);
		//wcstombs(&buff[0], file_path, size);
		filenames[n] = buff;
		n++;
	} while (FindNextFile(hFind, &ffd) != 0);
	FindClose(hFind);

	//Figure out how many images we have to deal with for class 2
	PathCombine(wildcard_path, c2, star);
	//PathCchCombine(wildcard_path, MAX_PATH, c2, star);//change as appropriate
	hFind = FindFirstFile(wildcard_path, &ffd);
	if (hFind == INVALID_HANDLE_VALUE)
	{
		//This is not good.
	}
	do{
		PathCombine(file_path, c2, ffd.cFileName);
		//PathCchCombine(file_path, MAX_PATH, c2, ffd.cFileName);
		size = (wcslen(file_path + 1) * sizeof(wchar_t));
		wcstombs_s(&size, buff, max, file_path, max);
		//wcstombs(&buff[0], file_path, size);
		filenames[n] = buff;
		n++;
	} while (FindNextFile(hFind, &ffd) != 0);
	FindClose(hFind);*/

	cout << "Getting image area\n\n";
	int num_files = n;
	int img_h = IMAGE_HEIGHT; int img_w = IMAGE_WIDTH; 
	int img_area = img_h*img_w;
	cout << "Building Training Mat and Labels mat\n\n";
	Mat training_mat(num_files, img_area, CV_32FC1);
	Mat labels(num_files, 1, CV_32FC1);
	
	//Now take each image (a two D matrix) and transform it into a 1D matrix
	//[1 2 3
	// 4 5 6
	// 7 8 9]
	//becomes [1 2 3 4 5 6 7 8 9]
	cout << "Building img_mat\n\n";
	Mat img_mat;
	//int ii=0;
	//for(int i=0; i<img_mat.rows;i++;){
	//	for(int j=0; j <img_mat.cols; j++({
	//	training_mat.at<float>(file_num,ii++)=img_mat.at<uchar>(i,j);
	//	}
	//}

	int ii = 0;//column of traning_mat
	int i = 0; //loop var
	int j = 0; //loop var
	cout << "Handling files 0-7\n\n";
	for (int k = 0; k < /*num_files*/ 8; k++)//for each image in class 0
	{
		cout << ("Opening " + filenames[k] + "\n\n");
		img_mat = imread(filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<uchar>(i, j);
			}
		}
		cout << ("Updating lables for " + filenames[k] + "\n\n");
		labels.rowRange(0, 8).setTo(0);//For CLASS 0
		//labels.at<float>(k, 1) = 0;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + filenames[k] + "\n\n");
	}

	ii = 0;//column of traning_mat
	i = 0; //loop var
	j = 0; //loop var
	cout << "Handling files 8-15\n\n";
	for (int k = 8; k < /*num_files*/ 16; k++)//for each image in class 1
	{
		cout << ("Opening " + filenames[k] + "\n\n");
		img_mat = imread(filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<uchar>(i, j);
			}
		}
		cout << ("Updating lables for " + filenames[k] + "\n\n");
		labels.rowRange(8, 16).setTo(1);//For CLASS 1
		//labels.at<float>(k, 1) = 1;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + filenames[k] + "\n\n");
	}

	ii = 0;//column of traning_mat
	i = 0; //loop var
	j = 0; //loop var
	cout << "Handling files 16-23\n\n";
	for (int k = 16; k < /*num_files*/ 24; k++)//for each image in class 2
	{
		cout << ("Opening " + filenames[k] + "\n\n");
		img_mat = imread(filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<uchar>(i, j);
			}
		}
		cout << ("Updating lables for " + filenames[k] + "\n\n");
		labels.rowRange(16, 24).setTo(2);//For CLASS 2
		//labels.at<float>(k, 1) = 1;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + filenames[k] + "\n\n");
	}

	//setup SVM parameters THESE WILL PROBABLY HAVE TO BE TUNED
	cout << "Setting up params\n\n";
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::POLY;
	params.gamma = 3;
	params.degree = 2;
	
	cout << "Prepare to train...\n\n";
	cout << "This will take a while, go get coffee...\n\n";
	//Get ready to wait for a year while this trains
	CvSVM svm;
	svm.train(training_mat, labels, Mat(), Mat(), params);

	//training complete, save training to a file
	svm.save("Training");

	cout << "Training Complete\n\n";
	cout << "Opening sample image in class 0\n\n";

	Mat iut_mat;
	Mat iut_mat_1d(1, img_area, CV_32FC1);
	iut_mat = imread("iut_000", 1);

	cout << ("Flattening iut_000\n\n");

	for (i = 0; i < iut_mat.rows; i++)
	{
		for (j = 0; j < iut_mat.cols; j++)
		{
			iut_mat_1d.at<float>(1, ii++) = iut_mat.at<uchar>(i, j);
		}
	}

	cout << "Running svm prediction 100 times, expecting zero\n\n";
	float svm_out = -42;
	for (i = 0; i < 100; i++)
	{
		svm_out = svm.predict(iut_mat_1d);
		cout << svm_out << "\n";
	}
}