#include <windows.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <chrono>
#include <future>
#include <ctime>
#include <winnt.h>
#include <thread>
#include <iostream>
#include <Shlwapi.h>

#define SAMPLE_RATE 30;
#define COLOR_IMAGE_WIDTH	290;
#define COLOR_IMAGE_HEIGHT	290;
#define DEPTH_IMAGE_WIDTH	90;
#define DEPTH_IMAGE_HEIGHT	90;
#define SVM_ERROR	-42
#define SAMPLE_PERIOD 1/SAMPLE_RATE

//float svm_decision(cv::Mat image_under_test);

//Make this part of an object, customized for each assembly wherein each assembly is an object
//enum objects{sq2blk,rec24blk,window,girl,car,assmcomp, assmstp1, assmstp2};