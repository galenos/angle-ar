#include "Header.h"

using namespace cv;
using namespace std;

int h = IMAGE_HEIGHT;
int w = IMAGE_WIDTH;

//Takes in an image as 1D array, returns the classification integer
//Flatten image using:
//Mat image_2D= imread(name,1);
//Mat image_under_test(1,IMAGE_WIDTH * IMAGE_HEIGHT, CV_32FC1);
//for (int i = 0; i<image_2D.rows; i++) {
//	for (int j = 0; j < image_2D.cols; j++) {
//		image_under_test.at<float>(0, 1) = image_2D.at<uchar>(i, j);
//	}
//}

float svm_decision(Mat image_under_test)
{
	float classification=SVM_ERROR;
	size_t temp = (size_t)h*w;
	CvSVM svm;
	svm.load("Training");
	//if (image_under_test.total == temp)
	//{
		svm.predict(image_under_test);
	//}

	return classification;
}