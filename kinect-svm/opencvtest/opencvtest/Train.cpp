#include "Header.h"
#include <Shlwapi.h>
using namespace cv;
using namespace std;



int main()
{

	//From Kinect

	//RGBQUAD *kinectGarbage_color;
	//Mat OpenCVGarbage_color(1080, 1920, CV_8UC3, reinterpret_cast<void*>(kinectGarbage_color));

	//RGBQUAD *kinectGarbage_depth;
	//Mat OpenCVGarbage_depth(424, 512, CV_8UC1, reinterpret_cast<void*>(kinectGarbage_color));
	//string filenames[1000];//limit of 1000 images per dir
	cout << "Listing filenames\n\n";
	//where X=0 -> 23

	//assembly_cX
	//blue_car_cX
	//blue_window_cX
	//nothing_cX
	//red_2x4x1_cX

	//assembly_dX where X=0 -> 23
	//blue_car_dX
	//blue_window_dX
	//nothing_dX
	//red_2x4x1_dX
	int n = 120;
	string color_filenames[120] =
	{
		"assembly_c0.bmp", "assembly_c1.bmp", "assembly_c2.bmp", "assembly_c3.bmp",
		"assembly_c4.bmp", "assembly_c6.bmp", "assembly_c7.bmp", "assembly_c8.bmp",
		"assembly_c9.bmp", "assembly_c10.bmp", "assembly_c11.bmp", "assembly_c12.bmp",
		"assembly_c13.bmp", "assembly_c14.bmp", "assembly_c15.bmp", "assembly_c16.bmp",
		"assembly_c17.bmp", "assembly_c18.bmp", "assembly_c19.bmp", "assembly_c20.bmp",
		"assembly_c21.bmp", "assembly_c22.bmp", "assembly_c23.bmp",
		"blue_car_c0.bmp", "blue_car_c1.bmp", "blue_car_c2.bmp", "blue_car_c3.bmp",
		"blue_car_c4.bmp", "blue_car_c6.bmp", "blue_car_c7.bmp", "blue_car_c8.bmp",
		"blue_car_c9.bmp", "blue_car_c10.bmp", "blue_car_c11.bmp", "blue_car_c12.bmp",
		"blue_car_c13.bmp", "blue_car_c14.bmp", "blue_car_c15.bmp", "blue_car_c16.bmp",
		"blue_car_c17.bmp", "blue_car_c18.bmp", "blue_car_c19.bmp", "blue_car_c20.bmp",
		"blue_car_c21.bmp", "blue_car_c22.bmp", "blue_car_c23.bmp",
		"blue_window_c0.bmp", "blue_window_c1.bmp", "blue_window_c2.bmp", "blue_window_c3.bmp",
		"blue_window_c4.bmp", "blue_window_c6.bmp", "blue_window_c7.bmp", "blue_window_c8.bmp",
		"blue_window_c9.bmp", "blue_window_c10.bmp", "blue_window_c11.bmp", "blue_window_c12.bmp",
		"blue_window_c13.bmp", "blue_window_c14.bmp", "blue_window_c15.bmp", "blue_window_c16.bmp",
		"blue_window_c17.bmp", "blue_window_c18.bmp", "blue_window_c19.bmp", "blue_window_c20.bmp",
		"blue_window_c21.bmp", "blue_window_c22.bmp", "blue_window_c23.bmp",
		"nothing_c0.bmp", "nothing_c1.bmp", "nothing_c2.bmp", "nothing_c3.bmp",
		"nothing_c4.bmp", "nothing_c6.bmp", "nothing_c7.bmp", "nothing_c8.bmp",
		"nothing_c9.bmp", "nothing_c10.bmp", "nothing_c11.bmp", "nothing_c12.bmp",
		"nothing_c13.bmp", "nothing_c14.bmp", "nothing_c15.bmp", "nothing_c16.bmp",
		"nothing_c17.bmp", "nothing_c18.bmp", "nothing_c19.bmp", "nothing_c20.bmp",
		"nothing_c21.bmp", "nothing_c22.bmp", "nothing_c23.bmp",
		"red_2x4x1_c0.bmp", "red_2x4x1_c1.bmp", "red_2x4x1_c2.bmp", "red_2x4x1_c3.bmp",
		"red_2x4x1_c4.bmp", "red_2x4x1_c6.bmp", "red_2x4x1_c7.bmp", "red_2x4x1_c8.bmp",
		"red_2x4x1_c9.bmp", "red_2x4x1_c10.bmp", "red_2x4x1_c11.bmp", "red_2x4x1_c12.bmp",
		"red_2x4x1_c13.bmp", "red_2x4x1_c14.bmp", "red_2x4x1_c15.bmp", "red_2x4x1_c16.bmp",
		"red_2x4x1_c17.bmp", "red_2x4x1_c18.bmp", "red_2x4x1_c19.bmp", "red_2x4x1_c20.bmp",
		"red_2x4x1_c21.bmp", "red_2x4x1_c22.bmp", "red_2x4x1_c23.bmp"
	};
	string depth_filenames[120] =
	{
		"assembly_d0.bmp", "assembly_d1.bmp", "assembly_d2.bmp", "assembly_d3.bmp",
		"assembly_d4.bmp", "assembly_d6.bmp", "assembly_d7.bmp", "assembly_d8.bmp",
		"assembly_d9.bmp", "assembly_d10.bmp", "assembly_d11.bmp", "assembly_d12.bmp",
		"assembly_d13.bmp", "assembly_d14.bmp", "assembly_d15.bmp", "assembly_d16.bmp",
		"assembly_d17.bmp", "assembly_d18.bmp", "assembly_d19.bmp", "assembly_d20.bmp",
		"assembly_d21.bmp", "assembly_d22.bmp", "assembly_d23.bmp",
		"blue_car_d0.bmp", "blue_car_d1.bmp", "blue_car_d2.bmp", "blue_car_d3.bmp",
		"blue_car_d4.bmp", "blue_car_d6.bmp", "blue_car_d7.bmp", "blue_car_d8.bmp",
		"blue_car_d9.bmp", "blue_car_d10.bmp", "blue_car_d11.bmp", "blue_car_d12.bmp",
		"blue_car_d13.bmp", "blue_car_d14.bmp", "blue_car_d15.bmp", "blue_car_d16.bmp",
		"blue_car_d17.bmp", "blue_car_d18.bmp", "blue_car_d19.bmp", "blue_car_d20.bmp",
		"blue_car_d21.bmp", "blue_car_d22.bmp", "blue_car_d23.bmp",
		"blue_window_d0.bmp", "blue_window_d1.bmp", "blue_window_d2.bmp", "blue_window_d3.bmp",
		"blue_window_d4.bmp", "blue_window_d6.bmp", "blue_window_d7.bmp", "blue_window_d8.bmp",
		"blue_window_d9.bmp", "blue_window_d10.bmp", "blue_window_d11.bmp", "blue_window_d12.bmp",
		"blue_window_d13.bmp", "blue_window_d14.bmp", "blue_window_d15.bmp", "blue_window_d16.bmp",
		"blue_window_d17.bmp", "blue_window_d18.bmp", "blue_window_d19.bmp", "blue_window_d20.bmp",
		"blue_window_d21.bmp", "blue_window_d22.bmp", "blue_window_d23.bmp",
		"nothing_d0.bmp", "nothing_d1.bmp", "nothing_d2.bmp", "nothing_d3.bmp",
		"nothing_d4.bmp", "nothing_d6.bmp", "nothing_d7.bmp", "nothing_d8.bmp",
		"nothing_d9.bmp", "nothing_d10.bmp", "nothing_d11.bmp", "nothing_d12.bmp",
		"nothing_d13.bmp", "nothing_d14.bmp", "nothing_d15.bmp", "nothing_d16.bmp",
		"nothing_d17.bmp", "nothing_d18.bmp", "nothing_d19.bmp", "nothing_d20.bmp",
		"nothing_d21.bmp", "nothing_d22.bmp", "nothing_d23.bmp",
		"red_2x4x1_d0.bmp", "red_2x4x1_d1.bmp", "red_2x4x1_d2.bmp", "red_2x4x1_d3.bmp",
		"red_2x4x1_d4.bmp", "red_2x4x1_d6.bmp", "red_2x4x1_d7.bmp", "red_2x4x1_d8.bmp",
		"red_2x4x1_d9.bmp", "red_2x4x1_d10.bmp", "red_2x4x1_d11.bmp", "red_2x4x1_d12.bmp",
		"red_2x4x1_d13.bmp", "red_2x4x1_d14.bmp", "red_2x4x1_d15.bmp", "red_2x4x1_d16.bmp",
		"red_2x4x1_d17.bmp", "red_2x4x1_d18.bmp", "red_2x4x1_d19.bmp", "red_2x4x1_d20.bmp",
		"red_2x4x1_d21.bmp", "red_2x4x1_d22.bmp", "red_2x4x1_d23.bmp"
	};

	cout << "Getting image area\n\n";
	int num_files = n;
	int img_h = DEPTH_IMAGE_HEIGHT; int img_w = DEPTH_IMAGE_WIDTH;
	int img_area = img_h*img_w;
	cout << "Building Training Mat and Labels mat\n\n";
	Mat training_mat(num_files, img_area, CV_32FC1);
	Mat labels(num_files, 1, CV_32FC1);
	//Now take each image (a two D matrix) and transform it into a 1D matrix
	//[1 2 3
	// 4 5 6
	// 7 8 9]
	//becomes [1 2 3 4 5 6 7 8 9]
	cout << "Building img_mat\n\n";
	Mat img_mat;
	//int ii=0;
	//for(int i=0; i<img_mat.rows;i++;){
	//	for(int j=0; j <img_mat.cols; j++({
	//	training_mat.at<float>(file_num,ii++)=img_mat.at<uchar>(i,j);
	//	}
	//}

	int ii = 0;//column of traning_mat
	int i = 0; //loop var
	int j = 0; //loop var
	cout << "Handling files 0-23\n\n";
	for (int k = 0; k <24; k++)//for each image in class 0
	{
		cout << ("Opening " + depth_filenames[k] + "\n\n");
		img_mat = imread(depth_filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + depth_filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<uchar>(i, j);
			}
		}
		cout << ("Updating lables for " + depth_filenames[k] + "\n\n");
		labels.rowRange(0, 23).setTo(0);//For CLASS 0
		//labels.at<float>(k, 1) = 0;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + depth_filenames[k] + "\n\n");
	}

	ii = 0;//column of traning_mat
	i = 0; //loop var
	j = 0; //loop var
	cout << "Handling files 24-47\n\n";
	for (int k = 24; k <48; k++)//for each image in class 1
	{
		cout << ("Opening " + depth_filenames[k] + "\n\n");
		img_mat = imread(depth_filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + depth_filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<uchar>(i, j);
			}
		}
		cout << ("Updating lables for " + depth_filenames[k] + "\n\n");
		labels.rowRange(24, 47).setTo(1);//For CLASS 1
		//labels.at<float>(k, 1) = 1;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + depth_filenames[k] + "\n\n");
	}

	ii = 0;//column of traning_mat
	i = 0; //loop var
	j = 0; //loop var
	cout << "Handling files 48-71\n\n";
	for (int k = 48; k <72; k++)//for each image in class 2
	{
		cout << ("Opening " + depth_filenames[k] + "\n\n");
		img_mat = imread(depth_filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + depth_filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<uchar>(i, j);
			}
		}
		cout << ("Updating lables for " + depth_filenames[k] + "\n\n");
		labels.rowRange(48, 71).setTo(2);//For CLASS 2
		//labels.at<float>(k, 1) = 1;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + depth_filenames[k] + "\n\n");
	}

	ii = 0;//column of traning_mat
	i = 0; //loop var
	j = 0; //loop var
	cout << "Handling files 72-95\n\n";
	for (int k = 72; k <95; k++)//for each image in class 3
	{
		cout << ("Opening " + depth_filenames[k] + "\n\n");
		img_mat = imread(depth_filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + depth_filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<uchar>(i, j);
			}
		}
		cout << ("Updating lables for " + depth_filenames[k] + "\n\n");
		labels.rowRange(72, 95).setTo(3);//For CLASS 3
		//labels.at<float>(k, 1) = 1;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + depth_filenames[k] + "\n\n");
	}

	ii = 0;//column of traning_mat
	i = 0; //loop var
	j = 0; //loop var
	cout << "Handling files 96-119\n\n";
	for (int k = 96; k <119; k++)//for each image in class 4
	{
		cout << ("Opening "+ depth_filenames[k] + "\n\n");
		img_mat = imread(depth_filenames[k], 1);//this needs to be done as a loop
		ii = 0; i = 0; j = 0;
		cout << ("Flattening " + depth_filenames[k] + "\n\n");
		for (i = 0; i < img_mat.rows; i++)
		{
			for (j = 0; j < img_mat.cols; j++)
			{
				training_mat.at<float>(k, ii++) = img_mat.at<float>(i, j);
			}
		}
		cout << ("Updating lables for " + depth_filenames[k] + "\n\n");
		labels.rowRange(96, 119).setTo(4);//For CLASS 4
		//labels.at<float>(k, 1) = 1;// 1 for CLASS 1 identifier.  2 for CLASS 2 identifier
		cout << ("Labels updated for " + depth_filenames[k] + "\n\n");
	}

	//setup SVM parameters THESE WILL PROBABLY HAVE TO BE TUNED
	cout << "Setting up params\n\n";
	CvSVMParams params;
	params.svm_type = CvSVM::C_SVC;
	params.kernel_type = CvSVM::POLY;
	params.coef0 = .1;
	params.gamma = .01;
	params.degree = 1;
	params.C = .1;

	cout << "Prepare to train...\n\n";
	cout << "This will take a while, go get coffee...\n\n";
	//Get ready to wait for a year while this trains
	CvSVM svm;
	svm.train(training_mat, labels, Mat(), Mat(), params);

	//training complete, save training to a file
	svm.save("Depth_Training");

	cout << "Training Complete\n\n";
}