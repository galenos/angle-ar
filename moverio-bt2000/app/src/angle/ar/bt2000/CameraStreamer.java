package angle.ar.bt2000;

import android.content.Context;
import android.hardware.Camera;

import java.util.concurrent.atomic.AtomicBoolean;

class CameraStreamer implements MessageHandler, Camera.PreviewCallback, Camera.DepthCallback {

    private static final String MESSAGE_TYPE_FRAME = "FRAME";
    private static final int WIDTH = 640;
    private static final int HEIGHT = 480;
    private static final int COLOR_LENGTH = HEIGHT * 3/2 * WIDTH;
    private static final int DEPTH_LENGTH = HEIGHT * WIDTH;

    private MainApplication mApplication;
    private Camera mCamera;
    private NetMessageServer mServer;

    private final AtomicBoolean frameRequest = new AtomicBoolean(false);
    private final byte[] previewBuffer = new byte[COLOR_LENGTH];
    private byte[] depthData;

    CameraStreamer(Context context) {
        mApplication = (MainApplication) context.getApplicationContext();
        mCamera = mApplication.getCamera();
        mServer = mApplication.getServer();

        Camera.Parameters params = mCamera.getParameters();
        params.setEpsonCameraMode(Camera.Parameters.EPSON_CAMERA_MODE_EDOF);
        params.setPreviewSize(640, 480);
        params.setPreviewFpsRange(7500, 7500);
        mCamera.setParameters(params);

        mCamera.addCallbackBuffer(previewBuffer);
        mCamera.setPreviewCallbackWithBuffer(this);
        mCamera.setDepthCallback(this);

        mCamera.startPreview();
        mCamera.startDepthStreaming();

        mServer.addMessageHandler(MESSAGE_TYPE_FRAME, this);
    }

    @Override
    public void onNetMessage(DevMessage message) {
        frameRequest.set(true);
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {
        mCamera.addCallbackBuffer(previewBuffer);
    }

    @Override
    public void onDepthMap(byte[] bytes, Camera camera) {
        depthData = bytes;

        if (frameRequest.compareAndSet(true, false)) {
            byte[] data = new byte[COLOR_LENGTH + DEPTH_LENGTH];
            System.arraycopy(previewBuffer, 0, data, 0, COLOR_LENGTH);
            System.arraycopy(depthData, 0, data, COLOR_LENGTH, DEPTH_LENGTH);
            mServer.sendMessage(new DevMessage(MESSAGE_TYPE_FRAME, data));
        }
    }
}
