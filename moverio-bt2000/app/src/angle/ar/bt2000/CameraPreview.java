package angle.ar.bt2000;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private MainApplication mApplication;

    private Camera mCamera = null;
    private SurfaceHolder mHolder = null;

    CameraPreview(Context context) {
        super(context);

        mApplication = (MainApplication) context.getApplicationContext();

        mCamera = mApplication.getCamera();
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Do Nothing
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Do nothing
    }
}
