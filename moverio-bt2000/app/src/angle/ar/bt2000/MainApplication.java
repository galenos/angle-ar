package angle.ar.bt2000;

import android.app.Application;
import android.hardware.Camera;

public class MainApplication extends Application {

    private static final int PORT = 8900;

    private Camera mCamera;
    private NetMessageServer mServer;
    private CameraPreview mPreview;
    private CameraStreamer mStreamer;

    @Override
    public void onCreate() {
        super.onCreate();

        mCamera = Camera.open();
        mServer = new NetMessageServer(this, PORT);
        mPreview = new CameraPreview(this);
        mStreamer = new CameraStreamer(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mCamera.release();
    }

    Camera getCamera() {
        return mCamera;
    }
    NetMessageServer getServer() {
        return mServer;
    }
    CameraPreview getPreview() {
        return mPreview;
    }
    CameraStreamer getStreamer() {
        return mStreamer;
    }
}
