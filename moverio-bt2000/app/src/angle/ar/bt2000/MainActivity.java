package angle.ar.bt2000;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class MainActivity extends AppCompatActivity {

    MainApplication mApplication = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        window.addFlags(0x80000000);

        mApplication = (MainApplication) getApplicationContext();
        CameraPreview preview = mApplication.getPreview();

        FrameLayout container = (FrameLayout) findViewById(R.id.camera_preview);
        container.addView(preview);
    }
}
