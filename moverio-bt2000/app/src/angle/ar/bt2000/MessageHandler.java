package angle.ar.bt2000;

interface MessageHandler {
    void onNetMessage(DevMessage message);
}
