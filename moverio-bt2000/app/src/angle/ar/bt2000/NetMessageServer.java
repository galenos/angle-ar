package angle.ar.bt2000;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;

import static android.content.ContentValues.TAG;

class NetMessageServer {

    private static final String MESSAGE_TAG_CONNECT = "CONNECT";

    private Context mApplication;
    private Handler mHandler;

    private ServerSocket mServer;
    private ConcurrentHashMap<String, MessageHandler> mHandlerMap = new ConcurrentHashMap<>();
    private LinkedBlockingQueue<DevMessage> mMessageQueue = new LinkedBlockingQueue<>();

    NetMessageServer(Context context, int port) {

        try {
            mApplication = context.getApplicationContext();
            mHandler = new Handler(mApplication.getMainLooper());

            mServer = new ServerSocket(port);

            new Thread(new ServerListener()).start();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    void sendMessage(DevMessage message) {
        mMessageQueue.add(message);
    }

    void addMessageHandler(String tag, MessageHandler handler) {
        mHandlerMap.put(tag, handler);
    }
    void removeMessageHandler(String tag) {
        mHandlerMap.remove(tag);
    }

    private static void writeString(DataOutputStream out, String text) throws IOException {
        byte[] buffer = text.getBytes("UTF-8");
        writeBytes(out, buffer);
    }
    private static void writeBytes(DataOutputStream out, byte[] buffer) throws IOException {
        Log.d(TAG, String.format("writeBytes %d", buffer.length));
        byte[] length = ByteBuffer.allocate(4).putInt(buffer.length).array();
        out.write(length);
        out.write(buffer);
    }
    private static String readString(DataInputStream in) throws IOException {
        return new String(readBytes(in), "UTF-8");
    }
    private static byte[] readBytes(DataInputStream in) throws IOException {
        byte[] buffer = new byte[4];
        in.readFully(buffer);
        int length = ByteBuffer.wrap(buffer).getInt();
        buffer = new byte[length];
        in.readFully(buffer);
        return buffer;
    }

    private class ServerListener implements Runnable {

        @Override
        public void run() {

            while (!mServer.isClosed()) {

                Socket client = null;
                try {
                    client = mServer.accept();

                    DataOutputStream out = new DataOutputStream(client.getOutputStream());
                    writeString(out, MESSAGE_TAG_CONNECT);
                    out.flush();

                    Thread reader = new Thread(new ClientReader(client));
                    Thread writer = new Thread(new ClientWriter(client));

                    reader.start();
                    writer.start();

                    reader.join();
                    writer.interrupt();
                    writer.join();

                } catch (IOException | InterruptedException e) {} finally {
                    try {
                        if (client != null) client.close();
                    } catch (IOException e) {}
                }
            }
        }
    }

    private class ClientReader implements Runnable {

        private Socket mClient;

        ClientReader(Socket client) {
            mClient = client;
        }

        @Override
        public void run() {

            DataInputStream in = null;
            try {
                in = new DataInputStream(mClient.getInputStream());

                while (!mClient.isClosed()) {

                    MessageTypes type = MessageTypes.valueOf(readString(in));
                    String tag = readString(in);

                    final MessageHandler handler = mHandlerMap.get(tag);

                    if (handler != null) {
                        final DevMessage message = new DevMessage(type, tag);
                        switch (type) {
                            case TAG:
                                break;
                            case JSON:
                                message.mData = new JSONObject(readString(in));
                                break;
                            case RAW:
                                message.mRawData = readBytes(in);
                        }
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                handler.onNetMessage(message);
                            }
                        });
                    }
                }

            } catch (IOException | JSONException e) {} finally {
                try {
                    if (in != null) in.close();
                } catch (IOException e) {}
            }
        }
    }
    private class ClientWriter implements Runnable {

        private Socket mClient;

        ClientWriter(Socket client) {
            mClient = client;
        }

        @Override
        public void run() {

            DataOutputStream out = null;
            try {
                out = new DataOutputStream(mClient.getOutputStream());

                while (!mClient.isClosed()) {
                    DevMessage message = mMessageQueue.take();
                    MessageTypes type = message.getType();

                    writeString(out, type.name());
                    writeString(out, message.getTag());

                    switch (type) {
                        case TAG:
                            break;
                        case JSON:
                            writeString(out, message.getData().toString());
                            break;
                        case RAW:
                            writeBytes(out, message.getRawData());
                            break;
                    }
                    out.flush();
                }

            } catch (IOException | InterruptedException e) {} finally {
                try {
                    if (out != null) out.close();
                } catch (IOException e) {}
            }
        }
    }
}
