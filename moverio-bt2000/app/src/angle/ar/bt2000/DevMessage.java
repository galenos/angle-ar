package angle.ar.bt2000;

import org.json.JSONObject;

class DevMessage {

    private MessageTypes mType;
    private String mTag;
    JSONObject mData;
    byte[] mRawData;

    DevMessage(MessageTypes type, String tag) {
        mType = type;
        mTag = tag;
    }
    public DevMessage(String tag) {
        mType = MessageTypes.TAG;
        mTag = tag;
    }
    public DevMessage(String tag, JSONObject data) {
        mType = MessageTypes.JSON;
        mTag = tag;
        mData = data;
    }
    DevMessage(String tag, byte[] data) {
        mType = MessageTypes.RAW;
        mTag = tag;
        mRawData = data;
    }

    MessageTypes getType() {
        return mType;
    }
    String getTag() {
        return mTag;
    }
    JSONObject getData() {
        return mData;
    }
    byte[] getRawData() {
        return mRawData;
    }
}
